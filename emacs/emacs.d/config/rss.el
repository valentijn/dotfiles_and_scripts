(use-package org :ensure t)

(defcustom elfeed-download-podcast-directory "~/usr/audio/podcast"
  "The base directory for downloading podcast for the elfeed RSS reader"
  :group 'elfeed)

(defcustom elfeed-music-directory "~/usr/audio/music"
  "The base directory for downloading podcast from elfeed RSS reader"
  :group 'elfeed)

;;insert space before elfeed filter
(defun bjm/elfeed-search-live-filter-space ()
  "Insert space when running elfeed filter"
  (interactive)
  (let ((elfeed-search-filter (concat elfeed-search-filter " ")))
    (elfeed-search-live-filter)))

(defun my/mark-as-unread ()
  (interactive)
  (elfeed-search-tag-all 'unread))

(defun my/watch-in-mpv (url)
  "Watch a video in mpv"
  (call-process-shell-command (concat "mpv '" url "' &") nil 0))

(defun my/download-video (url directory)
  "Download a video in the specified directory"
  (save-window-excursion
    (async-shell-command
     (concat "youtube-dl \"" url "\" -x --add-metadata "
             "--audio-format mp3 -o \"" directory "/%(title)s.%(ext)s\"")
     "*download-log*")))

(defun my/elfeed-download-audio (type-directory)
  "Download the audio from a video and place in a directory with the name of the feed"
  (let* ((entry (elfeed-search-selected :single)) ;; selected item
         (feed-title (elfeed-feed-title (elfeed-entry-feed entry))) ;; title of the item
         (directory (concat type-directory "/" feed-title)) ;; directory
         (enclosures (elfeed-entry-enclosures entry))) ;; enclosed files

    ;; Create the directory for the podcast if it didn't already exist
    (if (not (file-exists-p directory))
        (make-directory directory))

    ;; Check if the video is enclosed instead of the url itself.
    (if enclosures
        (my/download-video (caar enclosures) directory)
      (my/download-video (elfeed-entry-link entry) directory))

    ;; mark as the item as read
    (elfeed-search-untag-all-unread)))

(defun my/elfeed-download-music ()
  "Rips a song from youtube and places it in the correct directory"
  (interactive)
  (my/elfeed-download-audio elfeed-music-directory))

(defun my/elfeed-download-podcast ()
  "Download a podcast episode with an appropriate name and in an appropiate directory."
  (interactive)
  (my/elfeed-download-audio elfeed-download-podcast-directory))

(defun my/elfeed-mpv ()
  (interactive)
  (let ((entry (elfeed-search-selected :single)))
    (my/watch-in-mpv (elfeed-entry-link entry))
    (elfeed-search-untag-all-unread)))

(defun my/open-in-gui ()
  (interactive)
  (let ((browse-url-generic-program "/usr/bin/xdg-open"))
    (save-excursion (elfeed-search-untag-all-unread))
    (elfeed-search-browse-url t)))

(defun my/search-browse-url ()
  (interactive)
  (save-excursion
    (elfeed-search-untag-all-unread))

  (save-current-buffer
    (split-window-sensibly (selected-window))
    (other-window 1))
  (elfeed-search-browse-url))

(use-package elfeed :ensure t
  :bind (:map elfeed-search-mode-map
              ("p" . my/elfeed-mpv)
              ("/" . bjm/elfeed-search-live-filter-space)
              ("R" . my/mark-as-unread)
              ("b" . my/search-browse-url)
              ("B" . my/open-in-gui)
              ("d" . my/elfeed-download-podcast)
              ("D" . my/elfeed-download-music)))

(use-package elfeed-org :ensure t
  :config
  (setq rmh-elfeed-org-files '("~/.dots/feeds.org"))
  (elfeed-org))

(use-package elfeed-goodies :ensure t :defer t
  :init (setq elfeed-goodies/entry-pane-position 'bottom)
  :config (elfeed-goodies/setup))
