;;;
;;; Initialize the correct files like a pro
;;;

(use-package info)
(use-package diminish :ensure t)
(use-package bind-key :ensure t)
(use-package markdown-mode :ensure t)
(use-package realgud :ensure t :defer t)
(use-package restclient :ensure t :defer t)

;; Comment do what I mean
(use-package comment-dwim-2
  :ensure t
  :defer t
  :bind (("M-;" . comment-dwim-2)))

;; Nim
(use-package nim-mode :ensure t
  :config (add-hook 'nim-mode-hook 'nimsuggest-mode))

(defun my/quickrun ()
  (interactive)
  (save-buffer)
  (quickrun))

(use-package quickrun :ensure t
  :bind (("<f5>" . #'my/quickrun)))

;; Magit
(use-package magit
  :ensure t
  :defer t
  :init (setq magit-commit-arguments '("--verbose" "--gpg-sign=3472D06EC721BD07"))

  :config
  (defadvice magit-status (around magit-fullscreen activate)
    (window-configuration-to-register :magit-fullscreen)
    ad-do-it
    (delete-other-windows))

  (defun magit-quit-session ()
    "Restores the previous configuration and kills the magit buffer"
    (interactive)
    (kill-buffer)
    (jump-to-register :magit-fullscreen))

  (defun magit-toggle-whitespace ()
    (interactive)
    (if (member "-w" magit-diff-options)
        (magit-dont-ignore-whitespace)
      (magit-ignore-whitespace)))

  (defun magit-ignore-whitespace ()
    (interactive)
    (add-to-list 'magit-diff-options "-w")
    (magit-refresh))

  (defun magit-dont-ignore-whitespace ()
    (interactive)
    (setq magit-diff-options (remove "-w" magit-diff-options))
    (magit-refresh))

  :bind (("C-c g s" . magit-status)
         :map magit-status-mode-map
         ("W" . magit-toggle-whitespace)
         ("q" . magit-quit-session)))

;; Better parenthesis (check docs)
(use-package smartparens
  :ensure t
  :init (smartparens-global-mode)

  :bind (("C-M-a" . sp-beginning-of-sexp)
         ("C-M-e" . sp-end-of-sexp))

  :diminish smartparens-mode
  :config (progn (require 'smartparens-config)
                 (sp-with-modes '(c-mode c++-mode d-mode web-mode)
                   (sp-local-pair "{" nil :post-handlers '(("||\n[i]" "RET")))
                   (sp-local-pair "/* " " */" :post-handlers '((" | ")
                                                               ("* ||\n[i]" "RET"))))))

;; Projectile, still haven't played with this
(use-package projectile
  :ensure t
  :defer t
  :diminish projectile-mode
  :init  (projectile-global-mode)
  :config
  (use-package counsel-projectile :ensure t)

  (setq projectile-enable-caching t
        projectile-switch-project-action 'projectile-find-dir
        projectile-keymap-prefix (kbd "C-c C-p")))

;; Auto indentation
(use-package aggressive-indent
  :ensure t
  :init (progn (global-aggressive-indent-mode 1)
               (add-to-list 'aggressive-indent-excluded-modes 'elm-mode)
               (add-to-list 'aggressive-indent-excluded-modes 'nim-mode)
               (add-to-list 'aggressive-indent-excluded-modes 'ivy-mode)
               (add-to-list 'aggressive-indent-excluded-modes 'haskell-mode)
               (add-to-list 'aggressive-indent-excluded-modes 'haskell-cabal-mode)
               (add-to-list 'aggressive-indent-excluded-modes 'asm-mode)
               (add-to-list 'aggressive-indent-excluded-modes 'python-mode)
               (setq aggressive-indent-comments-too t)))

(use-package rust-mode :ensure t :defer t :disabled t
  :init
  (add-hook 'rust-mode-hook
            #'(lambda () (set (make-local-variable 'compile-command)
                         (concat "cargo build && cargo run"))))
  :config
  (add-hook 'racer-mode-hook #'eldoc-mode)

  (use-package cargo :ensure t :defer t
    :init (add-hook 'rust-mode-hook 'cargo-minor-mode)
    :diminish cargo-mode)

  (use-package racer :ensure t :defer t
    :init (add-hook 'rust-mode-hook #'racer-mode)
    :diminish racer-mode)

  (use-package flycheck-rust :ensure t :defer t
    :init (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)))

;; Highlight indentation
(use-package indent-guide
  :diminish indent-guide-mode
  :ensure t
  :init (add-hook 'prog-mode-hook #'indent-guide-mode)
  :config (setq highlight-indent-guides-method 'character))

(use-package multi-compile
  :ensure t
  :defer t
  :init (setq multi-compile-alist
              '((scheme-mode . (("guile" . "guile -e main -s %file-name"))))))

;; Autocompletion, pretty general
(use-package company
  :diminish company-mode
  :ensure t
  :init (progn (add-hook 'prog-mode-hook #'company-mode)
               (setq company-show-numbers                   t
                     company-idle-delay                     0.8 ; decrease the delay
                     company-tooltip-limit                  1   ; make the popup bigger
                     company-tooltip-align-annotations      t   ; align annotations to the right
                     company-auto-complete                  t
                     company-quickhelp-use-propertized-text t

                     ;; weight by frequency
                     company-transformers '(company-sort-by-occurrence)))
  :config
  (use-package company-quickhelp :ensure t
    :config (setq company-frontends '(company-preview-if-just-one-frontend
                                      company-pseudo-tooltip-frontend
                                      company-echo-metadata-frontend
                                      company-quickhelp-frontend)))
  ;; <return> is for windowed Emacs; RET is for terminal Emacs
  (dolist (key '("<return>" "RET"))
    ;; Here we are using an advanced feature of define-key that lets
    ;; us pass an "extended menu item" instead of an interactive
    ;; function. Doing this allows RET to regain its usual
    ;; functionality when the user has not explicitly interacted with
    ;; Company.
    (define-key company-active-map (kbd key)
      `(menu-item nil company-complete
                  :filter ,(lambda (cmd)
                             (when (company-explicit-action-p)
                               cmd)))))
  (define-key company-active-map (kbd "TAB") #'company-complete-selection)
  (define-key company-active-map (kbd "SPC") nil)

  ;; Company appears to override the above keymap based on company-auto-complete-chars.
  ;; Turning it off ensures we have full control.
  (setq company-auto-complete-chars nil)

  (use-package pos-tip :ensure t)
  (use-package company-yasnippet
    :config
    ;; Add yasnippet support to all company  backends
    (defun company-mode/backend-with-yas (backend)
      (if (and (listp backend) (member 'company-yasnippet backend)) backend
        (append (if (consp backend) backend (list backend))
                '(:with company-yasnippet))))

    (setq company-backend (mapcar #'company-mode/backend-with-yas company-backends))))

;; Snippets
(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :init (yas-global-mode 1)
  :config (setq yas-snippet-dirs '("~/.emacs.d/elpa/yasnippet-20170226.1638/snippets"
                                   "~/.emacs.d/snippets")))

;; Autocheck code for emacs
(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :config (add-hook 'prog-mode-hook  #'flycheck-mode))

(use-package flycheck-color-mode-line
  :ensure t
  :init (add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode))

;; Language agnostic code related stuff
(setq compilation-always-kill t
      compilation-scroll-output 'first-error)
(use-package rainbow-identifiers :ensure t :config (add-hook 'prog-mode-hook 'rainbow-identifiers-mode))

(use-package nim-mode :ensure t :defer t)
(use-package lua-mode :ensure t :defer t)
(load "~/.emacs.d/config/languages/python.el")
(load "~/.emacs.d/config/languages/c.el")
(load "~/.emacs.d/config/languages/asm.el")
(load "~/.emacs.d/config/languages/web-dev.el")
(load "~/.emacs.d/config/languages/lisp.el")
;; (load "~/.emacs.d/config/languages/proglang.el")
;;(load "~/.emacs.d/config/languages/lsp.el")
;; (load "~/.emacs.d/config/languages/java.el")
(use-package eglot)
