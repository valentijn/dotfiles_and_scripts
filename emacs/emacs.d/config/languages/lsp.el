(use-package lsp-mode :ensure t :defer t
  :init
  (use-package lsp-ui :ensure t
    :config
    (setq lsp-ui-doc-enable t
          lsp-ui-sideline-enable nil
          lsp-ui-flycheck-enable t))
  (use-package company-lsp :ensure t)
  ;; (use-package lsp-python :ensure t)
  (use-package lsp-haskell :ensure t :disabled t)
  (use-package lsp-metals :ensure t :disabled t)

  :config
  (use-package lsp-ivy :ensure t :disabled t)
  (push 'company-lsp company-backend)
  (add-hook 'lsp-mode-hook 'lsp-ui-mode)
  (add-hook 'python-mode-hook #'lsp)
  (add-hook 'lsp-mode-hook #'(lambda () (aggressive-indent-mode -1)))
  (electric-indent-mode -1)
  (setq lsp-enable-on-type-formatting nil)
  (setq gc-cons-threshold 100000000)
  (setq read-process-output-max (* 1024 1024))
  (setq lsp-prefer-capf t)
  (setq lsp-prefer-flymake nil))


;; (use-package lsp-java :ensure t :after lsp
;;   :config
;;   (add-hook 'java-mode-hook #'lsp))

;; (use-package dap-mode :ensure t :after lsp-mode
;;   :config
;;   (dap-mode t)
;;   (dap-ui-mode t))

;; (use-package dap-java :after (lsp-java))

(use-package eglot :ensure t :defer t
  :config
  (add-hook 'c++-mode-hook 'eglot-ensure)

  (defun projectile-project-find-function (dir)
    (let* ((root (projectile-project-root dir)))
      (and root (cons 'transient root))))

  (with-eval-after-load 'project
    (add-to-list 'project-find-functions 'projectile-project-find-function))

  (setq company-backends
        (cons 'company-capf
              (remove 'company-capf company-backends)))

  (defun eglot-ccls-inheritance-hierarchy (&optional derived)
    "Show inheritance hierarchy for the thing at point.
If DERIVED is non-nil (interactively, with prefix argument), show
the children of class at point."
    (interactive "P")
    (if-let* ((res (jsonrpc-request
                    (eglot--current-server-or-lose)
                    :$ccls/inheritance
                    (append (eglot--TextDocumentPositionParams)
                            `(:derived ,(if derived t :json-false))
                            '(:levels 100) '(:hierarchy t))))
              (tree (list (cons 0 res))))
        (with-help-window "*ccls inheritance*"
          (with-current-buffer standard-output
            (while tree
              (pcase-let ((`(,depth . ,node) (pop tree)))
                (cl-destructuring-bind (&key uri range) (plist-get node :location)
                  (insert (make-string depth ?\ ) (plist-get node :name) "\n")
                  (make-text-button (+ (point-at-bol 0) depth) (point-at-eol 0)
                                    'action (lambda (_arg)
                                              (interactive)
                                              (find-file (eglot--uri-to-path uri))
                                              (goto-char (car (eglot--range-region range)))))
                  (cl-loop for child across (plist-get node :children)
                           do (push (cons (1+ depth) child) tree)))))))
      (eglot--error "Hierarchy unavailable"))))
