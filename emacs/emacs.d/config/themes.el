;;; Package --- Summary
;;; provides configuration for the theme my emacs runs
;;;
;;; Commentary:
;; Setup line highlighter

;;; Code:
;; Modeline configuration
(use-package git-gutter-fringe
  :ensure t
  :diminish git-gutter-mode
  :config (progn (global-git-gutter-mode +1)
                 (setq-default indicate-buffer-boundaries 'left)
                 (setq-default indicate-empty-lines +1)))

(use-package spaceline
  :ensure t
  :disabled t
  :config
  (require 'spaceline-config)
  (spaceline-spacemacs-theme)

  ;; Disable spaceline methods
  (spaceline-toggle-buffer-size-off)
  (spaceline-toggle-buffer-encoding-abbrev-off)
  (spaceline-toggle-projectile-root-on))

(use-package telephone-line
  :ensure t
  :disabled t
  :init
  (telephone-line-mode 1)
  (setq telephone-line-lhs
        '((accent . (telephone-line-vc-segment
                     telephone-line-erc-modified-channels-segment
                     telephone-line-process-segment))
          (nil    . (telephone-line-minor-mode-segment
                     telephone-line-buffer-segment
                     telephone-line-nyan-segment)))))

(set-face-attribute 'default nil :font "Hack" :height 96)
(set-face-attribute 'Info-quoted nil :slant 'italic :weight
                    'light :foundry "source" :family "code")

(setq inhibit-compacting-font-caches t)

;; Setup all the fonts
(use-package all-the-icons :defer t :ensure t
  :config (setq inhibit-compacting-font-caches t))

;; setup theme
(use-package sourcerer-theme
  :ensure t
  :disabled t
  :config (load-theme 'sourcerer t))

(use-package doom-themes
  :ensure t
  :init (setq doom-themes-enable-bold t
              doom-themes-enable-italic t
              doom-vibrant-comment-bg nil
              doom-vibrant-brighter-modeline t
              doom-vibrant-brighter-comments t
              doom-vibrant-padded-modeline nil)
  :config (progn (load-theme 'doom-vibrant t)
                 (doom-themes-visual-bell-config)
                 (doom-themes-neotree-config)))

(use-package color-theme-sanityinc-tomorrow :ensure t :disabled t
  :config
  (load-theme 'sanityinc-tomorrow-eighties t)

  ;; Make the line highlighted italic
  (set-face-attribute
   'hl-line nil
   :weight 'bold :background nil)

  ;; Show the opposite parenthesis
  (set-face-attribute
   'show-paren-match nil
   :background "#515151" :foreground nil)

  ;; make the company-quickhelp popups a little more tolerable
  (setq company-quickhelp-color-foreground "#cccccc"
        company-quickhelp-color-background "#2d2d2d"))

(use-package solaire-mode :ensure t :defer t
  :hook
  ((change-major-mode after-revert ediff-prepare-buffer) . turn-on-solaire-mode)
  (minibuffer-setup . solaire-mode-in-minibuffer)
  :config
  (solaire-global-mode +1)
  (solaire-mode-swap-bg)
  (setq solaire-mode-remap-modeline nil))

(defun config/program-ui ()
  (hl-line-mode 1))

(add-hook 'prog-mode-hook #'config/program-ui)
