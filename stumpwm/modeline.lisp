;; Bar variables
(load "~/.config/stumpwm/gpmdp.lisp")

(defvar *power-path* "/sys/class/power_supply/BAT0/" "The path to where the battery is located")
(defvar *power-icon* nil "The symbol to display the state of the battery")

(defcommand music-info () ()
  (run-function))

(defcommand run-battery () ()
  (setf *battery-status*
        (string-right-trim '(#\Newline)
                           (stumpwm:run-shell-command
                            (concatenate 'string "cat " *power-path* "status") t))

        *battery-level*
        (parse-integer
         (string-right-trim '(#\Newline)
                            (stumpwm:run-shell-command
                             (concatenate 'string "cat " *power-path* "capacity") t))))

  (if (equal *battery-status* "Charging") (setf *power-icon* "")
    (cond
     ((>= *battery-level* 100) (setf *power-icon* ""))
     ((> *battery-level* 75)   (setf *power-icon* ""))
     ((> *battery-level* 50)   (setf *power-icon* ""))
     ((> *battery-level* 25)   (setf *power-icon* ""))
     ((< *battery-level* 25)   (setf *power-icon* ""))))

  (format nil "~A ~D%" *power-icon* (if (>= *battery-level* 100) 100 *battery-level*)))

(setf stumpwm:*screen-mode-line-format*
      (list "[^7%n^]]" ;; current group in white
            " %v"
            ;;      " - ^13%u^n"
            "^>^7" ;; align right
            " "
            '(:eval (music-info))
            "    "
            '(:eval
              (read-line (open "~/tmp/email")))
            "  "
            '(:eval (run-battery))
            "   "
            '(:eval (stumpwm:run-shell-command "date '+%d/%m/%Y %R'" t))))

;; Set the pointer to a more sensible theme
;;(setq *grab-pointer-character* 40
  ;;    *grab-pointer-character-mask* 41
    ;;  *grab-pointer-foreground* (hex-to-xlib-color "#3db270")
;;      *grab-pointer-background* (hex-to-xlib-color "#2c53ca")
;;      *mode-line-timeout* 0)
