#!/usr/bin/guile \
-e main -s
!#
(use-modules (ice-9 ftw))

;; Make randomness actually random
(set! *random-state* (random-state-from-platform))

(define nsfw #f)
(define background-mode "--bg-fill")

;; Get all images recursively from a directory
(define (get-image directory)
  ;; Actually do the work
  (define (get-images-helper directory files)
    ;; Check if the list is empty first so we don't get an error message at the end
    ;; TODO if length = 1 do something?
    (if (null? files)
        '()
        ;; Create the full path to the file so we can load it later.
        (let ((file (string-append directory (car files))))
          (cond
           ;; Skip useless directories which will cause infinite recursion
           ((or (string=? (car files) ".")
                (string=? (car files) ".."))
            (get-images-helper directory (cdr files)))

           ;; Check if the file is a directory, if so run the function again on the new directory.
           ((eqv? 'directory (stat:type (stat file)))
            (append (get-images-helper (string-append file "/") (scandir file))
                    (get-images-helper directory (cdr files))))

           ;; If not found it should be an image file so you can send it back
           (else (cons file (get-images-helper directory (cdr files))))))))

  ;; Allow for both name and name/ and send the correct directory to the helper function
  (let ((fullname (string-append (getenv "HOME") "/usr/img/wall/" directory)))
    (if (string=? (string-take-right directory 1) "/")
        (get-images-helper fullname (scandir fullname))
        (get-images-helper (string-append fullname "/") (scandir fullname)))))

;; Pick a random image from the list of full path images
(define (get-random-image files)
  (list-ref files (random (length files))))

;; Set images using background-mode (see feh (1))
(define (set-image image)
  (system (string-append "feh" " " background-mode " \"" image "\"")))

(define (main args)
  ;; Cache the files in the directory so we don't waste too much time doing useless computing
  ;; These change very infrequently so it really shouldn't matter.
  (let ((files (get-image "comp"))
        (anime (get-image "anime/pics"))
        (ntfs (get-image "anime/ntfs"))
        (gen (get-image "gen"))
        (pony (get-image "pony")))

    ;; Cache the time calculation.
    (while #t
      (let ((current-time (tm:hour (localtime (current-time)))))
        ;; Pick the correct list of images depending one time
        (cond [(or (>= current-time 23) (<= current-time 7))
               (set-image (if nsfw (get-random-image (append pony ntfs anime gen))
                              (get-random-image (append pony anime gen))))]

              [(or (>= current-time 17) (<= current-time 23)) (set-image
                                                               (get-random-image
                                                                (append gen anime)))]
              [else (set-image (get-random-image gen))])

        ;; sleep for ten minutes
        (sleep 600)))))
