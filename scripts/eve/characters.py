#!/usr/bin/python3
import eveapi
eveapi.set_user_agent("conky_eve")

KEY_ID = 4943609
VCODE  = "bVHfJEuT0BUMJmC8rlDpWNRlXmKhFSy4Qor5X9aJjOBc29r321DOtk90wQVqCj4Z"

api = eveapi.EVEAPIConnection()
auth = api.auth(keyID=KEY_ID, vCode=VCODE)

sheet = auth.char.CharacterSheet()
name = sheet.name
corpID = sheet.corporationID
corp = auth.corp.CorporationSheet(corporationID=corpID).ticker

print("{0} ({1})".format(name, corp))
