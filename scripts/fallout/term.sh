#!/bin/bash
run=0

clear
export PS1="> "
typeprint fallout/login
printf "> "; read
sleep 1.5
clear
#sed -i "s/foobar/$(whoami)/g" "fallout/intro
sed -i "s/foobar/Valentijn/g" "fallout/intro"
typeprint fallout/intro

while true; do
    if [ $run != 0 ]; then
	cat fallout/intro
    fi
    
    printf "\n        > "; read -rsn1 pick

    if [[ $pick = 1 ]]; then
	ncmpcpp
    elif [[ $pick = 2 ]]; then
	mutt
    elif [[ $pick = 3 ]]; then
	w3m "file://$HOME/src/www/start/index.html"
    elif [[ $pick = 4 ]]; then
	sed -i "s/$(whoami)/foobar/g" "fallout/intro"
	break
    fi

    run=$(($run + 1))
    clear
done

clear
    
