import datetime
import getpass
import glob
import random
import time
from subprocess import call

while True:
    hour = ''.join(str(datetime.datetime.now()).split()[1:])[:2]
    path = "/home/{}/Wallpaper/".format(getpass.getuser())

    if int(hour) > 16:
        path = glob.glob(path + "pics/*") + glob.glob(path + "general/*")
        picture = random.randint(0, len(path) - 1)
        call(["foobar.sh", path[picture]])
    elif int(hour) > 23 or int(hour) < 7:
        path = glob.glob(path + "vidya/*") + glob.glob(path + "comp/*") + glob.glob(path + "general/*")
        picture = random.randint(0, len(path) - 1)
        print(path[picture])
        call(["foobar.sh", path[picture]])
    else:
        path = glob.glob(path + "computers/*") + glob.glob(path + "general/*")
        picture = random.randint(0, len(path) - 1)
        call(["foobar.sh", path[picture]])

    time.sleep(600)
