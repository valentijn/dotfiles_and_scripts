###################
# Themes          #
###################
autoload -U promptinit; promptinit
setopt autocd
setopt prompt_subst  # Enable themes
prompt suse

if [ -n $BULLETTRAIN_PROMPT_CHAR ]; then
    BULLETTRAIN_STATUS_EXIT_SHOW=true
    BULLETTRAIN_TIME_SHOW=false
    BULLETTRAIN_EXEC_TIME_SHOW=true
fi
MINIMAL_INSERT_CHAR=""
MINIMAL_USER_CHAR="λ"
