#################################
# Named directories (very cool) #
#################################
img=~/usr/img/
vid=~/usr/vid/
opdr=~/usr/doc/opdr/
subj=~/opt/school/subj
wall=~/usr/img/wall/
music=~/usr/audio/music
books=~/usr/doc/books/
steam=~/.local/share/Steam/steamapps/common/
ports=~/usr/ports/
hyphan=~/src/pyth/hybot/
virtualenvs=~/src/pyth/virtualenvs/
: ~img ~vid ~opdr ~wall ~music ~books ~steam ~ports ~hyphan ~virtualenvs
