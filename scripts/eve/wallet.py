#!/usr/bin/python3
import eveapi

KEY_ID = 4943609
VCODE  = "bVHfJEuT0BUMJmC8rlDpWNRlXmKhFSy4Qor5X9aJjOBc29r321DOtk90wQVqCj4Z"

eveapi.set_user_agent("conky_eve")

api = eveapi.EVEAPIConnection()
auth = api.auth(keyID=KEY_ID, vCode=VCODE)
my_characters = auth.account.Characters()
isk = 0;

for character in my_characters.characters:
    wallet = auth.char.AccountBalance(characterID=character.characterID)
    isk += int(wallet.accounts[0].balance)

print('{0:,}'.format(isk))

