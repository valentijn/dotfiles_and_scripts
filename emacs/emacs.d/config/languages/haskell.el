;;;
;;; Haskell configuration
;;;

(use-package haskell-mode :ensure t :defer t
  :init (progn (require 'haskell-interactive-mode)
               (require 'haskell-process)
               (add-hook 'haskell-mode-hook #'interactive-haskell-mode)
               (add-hook 'haskell-mode-hook #'haskell-indentation-mode)
               (add-hook 'haskell-mode-hook #'haskell-doc-mode))

  :bind (:map haskell-mode-map
              ("C-c C-c" . haskell-compile)
              :map haskell-cabal-mode-map ("C-c C-c" . haskell-compile))
  :config
  (setq haskell-process-suggest-remove-import-lines t
        haskell-process-auto-import-loaded-modules t
        haskell-process-log t
        haskell-stylish-on-save t)

  (use-package flycheck-haskell :ensure t
    :init (add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))

  (use-package company-ghci :ensure t :init (push 'company-ghci company-backends)))
