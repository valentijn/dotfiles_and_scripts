;;;
;;; Configuration for org mode
;;;

(require 'org)
(require 'bind-key)

(bind-keys
 :map org-mode-map
 ("C-x C-o" . org-store-link)
 ("C-x C-a" . org-agenda))

;; Appearance
(setq org-hide-emphasis-markers nil
      org-hide-leading-stars    t
      org-fontify-whole-heading-line t
      org-highlight-latex-and-related '(latex script entities))
;; org-hidden-keywords '(author date email title)

;; Make writing code snippets in org a lot nicer
(setq org-confirm-babel-evaluate nil
      org-src-fontify-natively t
      org-src-tab-acts-natively t
      org-log-done 'time)

;; make standard keybinds org-mode aware
(setq org-special-ctrl-a t
      org-special-ctrl-k t)

;; don't collapse empty lines
(setq org-cycle-separator-lines 0
      org-plantuml-jar/path "/opt/plantuml/plantuml.jar"
      plantuml-jar-path "/opt/plantuml/plantuml.jar"
      org-plantuml-jar-path "/opt/plantuml/plantuml.jar"
      ;; plantuml-jar-path "/usr/share/plantuml/lib/plantuml.jar"
      plantuml-java-command "/usr/bin/plantuml")

(use-package plantuml-mode :ensure t
  :init (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
  :config (use-package flycheck-plantuml :ensure t
            :config (flycheck-plantuml-setup)))

;; (use-package ob-sml :ensure t :defer t)
(org-babel-do-load-languages
 'org-babel-load-languages
 '(;;(sh         . t)
   (css        . t)
   (plantuml   . t)
   (sql        . t)
   ;; (cypher . t)
   ;; (sml        . t)
   (C          . t)
   (python     . t)
   (emacs-lisp . t)))

(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))

(use-package org-bullets :ensure t
  :init (add-hook 'org-mode-hook #'org-bullets-mode))

(use-package org-ref :ensure t)
(require 'org-ref)
;; Latex exportation options
(setq org-latex-title-command "\\maketitle \\newpage")
(setq ;;org-latex-listings 'minted
 ;; org-latex-packages-alist '(("" "minted"))
 org-latex-packages-alist '(("" "apacite"))
 org-latex-minted-options '(("frame" "lines") ("linenos=true") ("bgcolor=gray!10"))
 org-latex-pdf-process '("xelatex -shell-escape -output-directory %o %f"))
