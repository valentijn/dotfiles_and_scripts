;;;
;;; Settings.lisp
;;; Settings for stumpwm
;;;

;; A nice startup message
;;(if (pathname-is-executable-p "/usr/bin/fortune")
;;    (setf *startup-message* (run-shell-command "fortune" t))
;;  (setf *startup-message* "Fortune (also known as fortunemod) is missing."))

(setf *input-history-ignore-duplicates* t          ;; ignore duplictes in the input bar.
;;      *shell-program*                   "zsh" ;; use zsh
      *ignore-wm-inc-hints*             t          ;; no gaps
      *mouse-focus-policy*              :sloppy    ;; Set focus on click
      *top-level-error-action*          :message   ;; Send an error messages instead of aborting
      *window-border-style*             :tight     ;; Make border TIGHT
      *window-format*                    "%s%m%t") ;; A decent layout for windows

;; Border color
(set-border-color "#376678")

;; Messages colors
(set-fg-color "#6d9eac")
(set-bg-color "#262729")

;; Redirect the output
(redirect-all-output (data-dir-file "stumpwm.log" "txt"))

;; Clear the rules
(clear-window-placement-rules)

;; Last rule to match takes precedence!
;; TIP: if the argument to :title or :role begins with an ellipsis, a substring
;; match is performed.
;; TIP: if the :create flag is set then a missing group will be created and
;; restored from *data-dir*/create file.
;; TIP: if the :restore flag is set then group dump is restored even for an
;; existing group using *data-dir*/restore file.
(define-frame-preference "Default"
  ;; frame raise lock (lock AND raise == jumpto)
  (1 t nil :class "XTerm"))

(define-frame-preference "Shareland"
  (0 t   nil :class "XTerm")
  (1 nil t   :class "aMule"))

(define-frame-preference "Emacs"
  (1 t t :restore "emacs-editing-dump" :title "...xdvi")
  (0 t t :create "emacs-dump" :class "Emacs"))
