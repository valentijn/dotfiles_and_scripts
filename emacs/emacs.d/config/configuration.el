;;;
;;; Bits of configuration that aren't part of a package being loaded
;;;

;; Some of it was stolen from technomacy's Better Defaults.
;; Most of these are self explanatory however use C-h v if you want the specifics.
;;; Code:
(setq select-enable-clipboard   t             ; normal clipboard behaviour
      select-enable-primary      t
      x-select-enable-clipboard-manager t
      confirm-kill-emacs #'y-or-n-p
      save-interprogram-paste-before-kill t
      ido-create-new-buffer           'always          ; don't prompt creating a new buffer
      recenter-positions '(top middle bottom)          ; make the recentering make sense.
      prettify-symbols-unprettify-at-point 'right-edge ; show the actual code when hovering
      apropos-do-all t
      mouse-yank-at-point t
      require-final-newline t                 ; always add a newline at the end of the file
      load-prefer-newer t                     ; load the newer files
      frame-title-format "%b %I %p"           ; filename
      fortune-dir                  "/usr/share/fortune"
      fortune-file                 "/usr/share/fortune/linuxcookie"
      kill-buffer-query-functions (remq 'process-kill-buffer-query-function
                                        kill-buffer-query-functions)
      ediff-window-setup-function 'ediff-setup-windows-plain
      save-place-file (concat user-emacs-directory "places")
      backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))


;; Set some default config tweaks
(setq indent-tabs-mode                    nil ; don't indent with tabs
      display-time-default-load-average   nil ; hide the load average
      calc-multiplication-has-precendence nil ; make arithmetic great again
      auto-revert-verbose                 nil ; quiet refresh
      confirm-nonexistent-file-or-buffer  nil ; don't confirm if a buffer doesn't exist
      global-auto-revert-non-file-buffers t   ; refresh dired
      shift-select-mode                   t   ; shift selects a region
      display-time-24hr-format            t   ; use the 24-hour clock
      tooltip-use-echo-area               t ; display pop-ups in the echo area
      redisplay-dont-pause                t   ; don't pause the display on input
      european-calendar                   t   ; sensible time
      show-paren-delay                    0   ; remove the delay
      calendar-week-start-day             1   ; weeks start on monday
      tab-width                           4
      show-paren-style          'expression   ; nice show expression
      tab-always-indent         'complete     ; sane tab
      initial-scratch-message   ";; This is Vaal's scratch for Elisp and such.\n\n"

      ;; smooth scrolling
      scroll-step                         1
      scroll-conservatively               1000
      mouse-wheel-scroll-amount '(1 ((shift) . 1)))

;; Enable modes
(global-prettify-symbols-mode 1) ; show pretty symbols instead of code in some instances.
(display-time-mode            1) ; show the time
(show-paren-mode              1) ; show matching parenthesis
(winner-mode                  1) ; undo window changes
(save-place-mode              1) ; save where I closed the buffer
(global-auto-revert-mode      1) ; auto refresh buffers
(scroll-bar-mode              0) ; disable the scrollbar
(menu-bar-mode                0) ; disable the menubar
(tool-bar-mode                0) ; disable the toolbar

(use-package popwin :ensure t :config (popwin-mode 1))

;; y or n instead of yes or no
(fset 'yes-or-no-p 'y-or-n-p)

;; IDO mode
(use-package ido-ubiquitous
  :disabled t
  :ensure t
  :config
  (ido-mode            1)
  (ido-everywhere      1)
  (ido-ubiquitous-mode 1))


(setenv "PATH" (concat "/home/faalentijn/.nimble/bin:" (getenv "PATH")))

(setq read-buffer-completion-ignore-case t
      ido-enable-flex-matching           t
      ido-use-faces                      nil)

;; add more files to the ignored extension list
(mapc (lambda (x)
        (add-to-list 'completion-ignored-extensions x))
      '(".aux" ".bbl" ".blg" ".exe"
        ".log" ".meta" ".out" "*~"
        ".synctex.gz" ".tdo" ".toc"
        "-pkg.el" "-autoloads.el"
        "Notes.bib" "auto/" "#*"))

;; Use shellscript mode to write Crux package files.
(add-to-list 'auto-mode-alist '("Pkgfile" . shell-script-mode))

;; Use the internal emacs password agent.
(setenv "GPG_AGENT_INFO" nil)
