;;;
;;; Holy fuck I'm tired and I swear I'll write decent docs for this some day.
;;; Yeah right... - Valentijn
;;;

;; Install the python packag1es
(require 'bind-key)
(require 'python)

(use-package elpy :ensure t :defer t)
(use-package pyvenv :ensure t)
(use-package pydoc-info :ensure t)
(use-package py-autopep8 :ensure t :defer t)

;; Use python across the board
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))

;; python configuration
(defun config/py-mode ()
  ;; Install elpy
  (elpy-enable)
  (highlight-indentation-mode -1)

  ;; (elpy-clean-modeline)
  (add-to-list 'company-backends '(elpy-company-backend :with company-yasnippet))

  ;; load the python completion environment
  ;; Start python interpreter

  (setq python-shell-interpreter "python"
        python-indent-guess-offset t
        ansi-color-for-comint-mode t
        python-shell-interpreter-args "-i")
  ;;(run-python "ipython" nil 0)

  (setq-local indent-tabs-mode t)
  (setq-local tab-width 4)
  (company-mode 0)

  ;; Use python to compile
  (set (make-local-variable 'compile-command)
       (concat "python " buffer-file-name))

  ;; better saving
  (add-hook 'after-save-hook #'config/py-cleanup nil t))

(defun config/py-cleanup ()
  (interactive)
  (untabify-buffer)
  (delete-trailing-whitespace)

  ;; Fix the imports
  ;;  (py-isort-buffer)
  ;;  (pyimport-remove-unused)

  ;; Run autopep8
  (py-autopep8))

(add-hook 'python-mode-hook #'config/py-mode)

(bind-keys
 :map python-mode-map
 :prefix-map custom-python
 :prefix "C-c C-p"
 ("b" . python-shell-send-buffer)
 ("s" . python-shell-send-string)
 ("c" . completion-at-point)
 ("e" . python-eldoc-at-point)
 ("f" . python-shell-send-file)
 ("r" . python-shell-send-region)
 ("v" . python-check))
