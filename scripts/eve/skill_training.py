#!/usr/bin/python3
import eveapi
import datetime
eveapi.set_user_agent("conky_eve")

# API login
KEY_ID = 4943609
VCODE  = "bVHfJEuT0BUMJmC8rlDpWNRlXmKhFSy4Qor5X9aJjOBc29r321DOtk90wQVqCj4Z"

api    = eveapi.EVEAPIConnection()
auth   = api.auth(keyID=KEY_ID, vCode=VCODE)
ID     = auth.char.CharacterSheet().characterID

# skill info
training   = auth.char.SkillInTraining(characterID=ID)
skillID    = training.trainingTypeID
skilllevel = training.trainingToLevel
timestamp  = training.trainingEndTime

# check the item database for the skill
for skill in api.eve.TypeName(ids=skillID).types.Select('typeName'):        
    print("{0} {1}".format(skill, skilllevel))
