#!/bin/bash
#
# Write all my programs to the screen for a screensaver
#

# directories to search
DIRS="$HOME/src/shell $HOME/src/pyth $HOME/src/www $HOME/src/c"

for dir in $DIRS; do
    FILES=$(find $dir -type f -name '*sh' -or -name '*py' -name '*html' -or -name '*css' -or -name '*.c')

    for file in $FILES; do
	echo "# This is the source code of $file"
	typeprint $file
	sleep 2
	clear
    done
done

