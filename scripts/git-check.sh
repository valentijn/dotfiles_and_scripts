#!/bin/bash
PREVENTION=0
PREVIOUS="nil"
COUNT=0
echo "0" > ~/tmp/git.tmp

cat ~/usr/doc/git_repos | while read x; do
    REPO=$(echo $x | awk '{print $1}')
    ORIGIN=$(echo $x | awk '{print $2}')
    BRANCH=$(echo $x | awk '{print $3}')
    cd "$REPO"

    GIT_NAME=$(git remote -v | head -n1 | awk '{print $2}' | sed -e 's,a.*:\(.*/\)\?,,' -e 's/\.git$//')

    git fetch
    git log --oneline $BRANCH > ~/tmp/git_orig
    git log --oneline $ORIGIN/$BRANCH > ~/tmp/git_new

    LINES_ORIG=$(cat ~/tmp/git_orig | wc --lines)
    LINES_NEW=$(cat ~/tmp/git_new | wc --lines)

    if [[ $LINES_ORIG != $LINES_NEW ]] && [[ $GIT_NAME != $PREVIOUS ]] ; then
	PREVIOUS=$GIT_NAME+$PREVIOUS
	NUMBER=$(($LINES_NEW - LINES_ORIG))
	COUNT=$(($(cat ~/tmp/git.tmp) + NUMBER))
	echo $COUNT > ~/tmp/git.tmp
    fi
done

echo $(cat ~/tmp/git.tmp)
