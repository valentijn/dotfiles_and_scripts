;;;
;;; mu4e for greater good.
;;;
;; Email client, alert and use multiple accounts
;;(use-package mu4e :init (el-get-bundle mu4e))
;; (use-package mu4e :ensure t)
;;(use-package mu4e-alert :ensure t)
;;(use-package mu4e-multi :init (el-get-bundle mu4e-multi))

;; Set the config. Use C-h v to read what they do.
(setq message-send-mail-function         'message-send-mail-with-sendmail
      send-mail-function                 'mailclient-send-it
      mail-user-agent                    'mu4e-user-agent
      message-kill-buffer-on-exit         t
      sendmail-program                   "msmtp"
      message-sendmail-f-is-evil          t
      starttls-use-gnutls                 t
      smtpmail-default-smtp-server       "posteo.de"
      smtpmail-smtp-server               "posteo.de"
      smtpmail-smtp-service               465
      mu4e-compose-dont-reply-to-self     t
      mu4e-update-interval                3000
      mu4e-maildir                        "~/.mail/posteo"
      mu4e-sent-messages-behavior         'delete
      mu4e-view-show-images               t
      mu4e-get-mail-command               "offlineimap -o"
      mu4e-decryption-policy              t
      mu4e-user-mail-address-list         nil
      mu4e-compose-complete-only-personal t
      mu4e-compose-signature-auto-include t
      mu4e-sent-folder                    "/Sent"
      mu4e-drafts-folder                  "/Drafts"
      mu4e-trash-folder                   "/Trash"
      mu4e-html2text-command              "html2text -utf8 -width 72"
      user-mail-address                   "valentijn@posteo.net"
      user-full-name                      "Valentijn"
      message-default-headers             "X-Clacks-Overhead: GNU Terry Pratchett"
      mu4e-maildir-shortcuts              '(("/INBOX"                        . ?i) ;; destination -> keybind
                                            ("/INBOX.GNU.Emacs Development"  . ?e)
                                            ("/INBOX.GNU.Emacs Help"         . ?h)
                                            ("/INBOX.GNU.Guile"              . ?u)
                                            ("/INBOX.GNU.GNU System Discuss" . ?d)
                                            ("/INBOX.GNU.Guix Development"   . ?x)
                                            ("/INBOX.GNU.Guix Help"          . ?z)
                                            ("/INBOX.Suckless"               . ?s)
                                            ("/INBOX.Gentoo"                 . ?g)
                                            ("/INBOX.PvdA"                   . ?p)))


;; Something to do with images
(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; Add an overhead to honour Terry Pratchett
(defun gnu-terry-pratchett ()
  "His name is in the code, in the wind in the rigging and the shutters."
  (save-excursion (message-add-header "X-Clacks-Overhead: GNU Terry Pratchett\n")))

;; Fucking lol.
(defun add-signature-mu4e ()
  "Brute force signature being added to the message"
  (save-excursion
    (end-of-buffer)
    (insert "\n---\n" (shell-command-to-string "fortune"))))

(add-hook 'mu4e-view-mode-hook (lambda () (beacon-mode -1)))

(add-hook 'mu4e-compose-mode-hook (lambda () (gnu-terry-pratchett) (add-signature-mu4e)))
