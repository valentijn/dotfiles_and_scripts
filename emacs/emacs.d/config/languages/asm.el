;;;
;;; Configuration for x86_64 assembly
;;;
;;; Code;
(use-package x86-lookup :ensure t :defer t
  :config (setq x86-lookup-pdf "~/Downloads/intel_x86.pdf"))
(defun my/asm-mode-hook ()
  (defun asm-calculate-indentation ()
    ;; Move assembler directives to the beginning of the line
    (and (looking-at "c?global\\|section\\|text\\|default\\|align\\|INIT_..X") 0)
    ;; Flush labels to the left margin.
    (and (looking-at "\\(\\sw\\|\\s_\\)+:") 0)
    ;; Same thing for `;;;' comments.
    (and (looking-at "\\s<\\s<\\s<") 0)
    ;; Simple `;' comments go to the comment-column.
    (and (looking-at "\\s<\\(\\S<\\|\\'\\)") comment-column)
    ;; The rest goes at the first tab stop.
    (or (indent-next-tab-stop 0)))
  (setq asm-comment-char ?\#)
  (setq-local tab-width 8)
  (electric-indent-local-mode -1)
  (linum-mode))

(add-hook 'asm-mode-hook #'my/asm-mode-hook)
