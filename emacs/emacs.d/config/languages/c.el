;;;
;;; The C Programming Language config
;;;
;; (global-set-key (kbd "<f5>")
;;                 (lambda ()
;;                   (interactive)
;;                   (setq-local compilation-read-command nil)
;;                   (call-interactively 'compile)))
;; C mode hook
(defun c-config ()
  (hide-ifdef-mode)
  ;; compile some straight forward C code
  (setq company-backends (delete 'company-semantic company-backends))
  ;; (irony-mode)

  (local-set-key (kbd "C-c c")   'company-clang)
  (local-set-key (kbd "C-c C-b") 'comment-box)
  (local-set-key (kbd "C-c C-a") 'cedit-beginning-of-statement)
  (local-set-key (kbd "C-c C-e") 'cedit-end-of-statement)
  (local-set-key (kbd "C-f")     'cedit-forward-char)
  (local-set-key (kbd "C-b")     'cedit-backward-char)
  (local-set-key (kbd "C-c w")   'cedit-wrap-brace)
  (local-set-key (kbd "M-RET")   'srefactor-refactor-at-point)

  (c-set-offset 'case-label '+)
  (c-set-offset 'cpp-macro 0)
  (c-set-offset 'cpp-macro-cont 1))

;; (use-package ccls :ensure t :defer t)

(defun cpp-config ()
  (hide-ifdef-mode)
  ;; (require 'ccls)
  (eglot-ensure)
  (c-set-offset 'access-label '+)
  (c-set-offset 'case-label '+)
  (c-set-offset 'cpp-macro 0)
  (c-set-offset 'cpp-macro-cont 1))


(add-hook 'c-mode-hook #'c-config)
(add-hook 'c++-mode-hook #'cpp-config)

;; programming modes configuration
(setq c-default-style     "stroustrup"
      c-tab-always-indent 'complete ; Tab insert in literals else indents
      gdb-many-windows     t        ; use gdb-many-windows by default
      gdb-show-main        t        ; display source file containing the main

      flycheck-checker    'c/c++-gcc
      flycheck-gcc-openmp   t
      flycheck-gcc-language-standard "gnu11")
