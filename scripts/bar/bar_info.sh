﻿#!/bin/bash
#
# Create a bar. This is /heavily/ modified version of Z3bra's bar.
# It also features some of bspwm's example script config
#
# variables
PANEL_FIFO=~/tmp/panel-fifo
PANEL_HEIGHT=20
bspc config top_padding $PANEL_HEIGHT

# check if the panel is running
if [ $(pgrep -cx panel) -gt 1 ]; then
    printf "%s\n" "The panel is already running." >&2
    exit 1
fi

trap 'trap - TERM; kill 0' INT TERM QUIT EXIT

# make a FIFO file and put some data in it
[ -e "$PANEL_FIFO" ] && rm "$PANEL_FIFO"
mkfifo "$PANEL_FIFO"

# make the space big enough
bspc control --subscribe > "$PANEL_FIFO" &
xtitle -sf 'T%s' > "$PANEL_FIFO" &
~/src/shell/run-git.sh > "$PANEL_FIFO" &
~/src/shell/email.sh > "$PANEL_FIFO" &

clock() {
    date '+%R'
}

batteryc() {    
    BATC=/sys/class/power_supply/BAT0/capacity
    STAT=$(sed -n p $BATC)

    if [ $STAT == 113 ]; then
	echo "100"
    else
	echo $STAT
    fi
    
}

batterys() {
    BATC=/sys/class/power_supply/BAT0/capacity
    BATS=/sys/class/power_supply/BAT0/status
    
    TES=$(cat $BATS)
    STAT=$(sed -n p $BATC)
    # prepend with a '' when charging and '' when not.
    if [[ $TES == "Full" ]] || [[ $TES == "Charging" ]]; then
	echo -n ''
    elif [[ $STAT > "75" ]]; then
	echo -n ''
    elif [[ $STAT > "50" ]]; then
	echo -n ''
    elif [[ $STAT > "25" ]]; then
	echo -n ''
    else
	echo -n ''
    fi
}
    
volume() {
    ponymix get-volume
}

packages() {
    if [ -f /usr/bin/pacman ]; then
	PACKAGES=$(pacman -Su --print-format %n | wc -l )
	echo $((PACKAGES - 1))
    fi
}

volumes() {
    VOLUME=$(ponymix get-volume)
    ponymix is-muted
    
    if [ $? == "0" ]; then
	echo -n ""
    else
	if [[ $VOLUME > "50" ]]; then
	    echo -n ""
	elif [[ $VOLUME > "1" ]]; then
	    echo -n ""
	else
	    echo -n ""
	fi
    fi
}

mail() {
    #    sed -i '3!d' $(cat ~/tmp/panel-fifo
    echo foo
}

desktop() {    
    echo "$(xprop -root _NET_CURRENT_DESKTOP | awk '{print $3}'): $(bspc query --desktops --desktop focused)"
}

music() {
    mpc status | grep paused &> /dev/null

    if [ $? == "0" ]; then
	echo "MPD paused"
    else
	mpc current
    fi
    
}

# This loop will fill a buffer with out infos, and output to stdout
while :; do
#    buf=""
#    buf="${buf} $(desktop)"
#    buf="${buf} %{c} $(xtitle)"
#    buf="${buf} %{r}   $(music) -"
#    buf="${buf}   $email -"
#    buf="${buf}   $(cat ~/tmp/git.tmp) - "
#    buf="${buf}   $(packages) -"
#    buf="${buf} $(volumes) $(volume)% -"
#    buf="${buf} $(batterys) $(batteryc)% -"
#    buf="${buf}   $(clock) "    
#    echo $buf    
    echo "D$(desktop)" > "$PANEL_FIFO"
    echo "M$(music)" > "$PANEL_FIFO"
    echo "P$(packages)" > "$PANEL_FIFO"
    echo "V$(volumes) $(volume)%" > "$PANEL_FIFO"
    echo "B$(batterys) $(batteryc)%" > "$PANEL_FIFO"
    echo "C$(clock)" > "$PANEL_FIFO"

    sleep 0.1
done
