#!/bin/sh
#
# z3bra - 2015 (c) wtfpl
# make the current window "rainbowish"... Awesome idea from xero@nixers.net !

FREQ=${FREQ:-1}
COLORS="#888888 #8064cc #6480cc #64cccc #80cc64 #cccc64 #cc6464"

while :; do
    for c in $COLORS; do
        riverctl border-color-focused $c
        sleep $FREQ
    done
done
