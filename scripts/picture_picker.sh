#!/bin/sh
#
# this script should open a picture and ask you if you want to save it
# this is for big archive files of wallpapers and such like.
number=0 # start the count
override=false # never over ride

# check the arguments
if [[ $1 == "-d" ]]; then
    DIR=$2
    PREFIX=$4   
else
    DIR=$4
    PREFIX=$2
fi

# try to deal with new lines
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

bspc rule -a sxiv floating=on

for x in $(find . -name '*jpg' -or -name '*png'); do
    format=$(echo "$x" | rev | cut -c -3 | rev)
    sxiv "$x" &
    sleep 0.1
    bspc window -f last &
    echo "Do you want to save this file? [N/y/r/l/f] "; read -rsn1 answer
    
    if [ $answer = "y" ] || [ $answer = "Y" ]; then
	cp "$x" $DIR/$PREFIX-$number.$format
	number=$((number + 1))
    elif [ $answer = "r" ] || [ $answer = "R" ]; then
	convert "$x" -rotate 90 $DIR/$PREFIX-$number.$format
	number=$((number + 1))
    elif [ $answer = "l" ] || [ $answer = "L" ]; then
	convert "$x" -rotate -90 $DIR/$PREFIX-$number.$format
	number=$((number + 1))
    elif [ $answer = "f" ] || [ $answer = "F" ]; then
	convert "$x" -flip $DIR/$PREFIX-$number.$format
	number=$((number + 1))
    fi

    killall sxiv &> /dev/null
done

# more new lines
IFS=$SAVEIFS
