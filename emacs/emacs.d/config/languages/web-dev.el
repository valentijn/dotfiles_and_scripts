;;;
;;; Download packages for web development
;;;
(use-package php-mode :ensure t :defer t :disabled t)

(use-package js2-mode :ensure t :defer t
  :init (progn (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
	       (add-to-list 'auto-mode-alist '("\\.mjs\\'" . js2-mode))
	       (add-hook 'js2-mode-hook #'js2-imenu-extras-mode))
  :config
  (use-package js2-refactor :ensure t :defer t
    :init (add-hook 'js2-mode-hook #'js2-refactor-mode)
    :bind (:map js2-mode-map
		("C-k" . js2r-kill))
    :config
    (js2r-add-keybindings-with-prefix "C-c C-r"))

  (use-package xref-js2 :ensure t :defer t
    :bind (:map js-mode-map
		("M-." . nil))
    :init (add-hook 'js2-mode-hook
		    (lambda () (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))))
(use-package web-mode :ensure t :defer t
  :init (progn (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
               (add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode))
	       (add-to-list 'auto-mode-alist '("\\.ejs?\\'" . web-mode))
               (setq web-mode-engines-alist '(("django"    . "\\.html\\'")
					      ("erb" . "\\.ejs\\'"))))

  :config (setq web-mode-markup-indent-offset 2
                web-mode-code-indent-offset 4
                web-mode-style-padding 1
                web-mode-script-padding 1
                web-mode-block-padding 0
                web-mode-attr-indent-offset 2
                web-mode-attr-value-indent-offset 2

                web-mode-enable-auto-expanding t
                web-mode-enable-sql-detection t
                web-mode-enable-auto-pairing t
                web-mode-enable-auto-closing t
                web-mode-enable-auto-quoting t
                web-mode-enable-css-colorization t
                web-mode-enable-comment-keywords t
                web-mode-enable-current-element-highlight t
                web-mode-enable-current-column-highlight t)

  (use-package company-web
    :config
    (add-to-list 'company-backends 'company-web-html)
    (add-to-list 'company-backends 'company-web-jade)
    (add-to-list 'company-backends 'company-web-css)))
