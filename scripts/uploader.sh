#!/bin/bash
#
# Basic uploader to a file upload server.
#
FILE_EXT=$(echo $@ |awk -F . '{if (NF>1) {print $NF}}')

if [[ `echo "$@" | cut -c 1-4` == "http" ]]; then
    URL=`curl -F c=@- https://ptpb.pw <<< "$@" | grep url | sed s/url:\ //g`
    FILE_EXT="web"
else
    URL=`curl -F c=@- https://ptpb.pw/ < "$@" | grep url  | sed s/url:\ //g `
fi

if [[ $FILE_EXT == "py" ]]; then 
    echo "$URL/py"
elif [[ $FILE_EXT == "c" ]]; then
    echo "$URL/C" 
elif [[ $FILE_EXT == "sh" ]]; then
    echo "$URL/bash" 
elif [[ $FILE_EXT == "web" ]]; then
    echo "$URL" 
else
   echo "$URL.$FILE_EXT" 
fi
   
