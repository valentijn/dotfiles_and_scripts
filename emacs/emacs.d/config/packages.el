;;;
;;; Install and configure packages using el-get and req-packages
;;;

;; load the require ackages
(use-package diminish :ensure t)
(use-package bind-key :ensure t)
(use-package info)
(use-package re-builder :defer t :config (setq reb-re-syntax 'rx))

;; use-package to verbose
(setq use-package-verbose nil)

(use-package beginend :ensure t :defer t
  :diminish beginend-global-mode
  :init (beginend-global-mode))

(use-package which-key :ensure t :defer t)

;; Show the cursor while scrolling or changing window
(use-package beacon :ensure t
  :diminish beacon-mode
  :init (beacon-mode 1))

;; Window manager for Emacs
(use-package eyebrowse :ensure t :init (eyebrowse-mode))

;; A package for easily moving around
(use-package avy :ensure t
  :bind (("C-c SPC" . avy-goto-char-2)
         ("C-c C-l" . avy-goto-line)))

;; Google this~
(use-package google-this :ensure t
  :init (google-this-mode 1)
  :config
  ;; Reconfigure google this to use Duck Duck Go
  (defun google-this-url ()
    (concat google-this-base-url google-this-location-suffix "/lite?q=%s"))
  (setq google-this-base-url "https://duckduckgo."
        google-this-location-suffix "org"
        google-this-wrap-in-quotes t
        google-this-modeline-indicator "")
  :diminish google-this-mode)

;; Bookmarks
(use-package bm :ensure t
  :config (setq bm-cycle-all-buffers t)
  :bind (("<C-f2>" . bm-toggle)
         ("<f2>"   . bm-next)
         ("<M-f2>" . bm-previous)
         ("<left-fringe> <mouse-1>" . bm-toggle-mouse)
         ("<left-fringe> <mouse-4>" . bm-previous-mouse)
         ("<left-fringe> <mouse-5>" . bm-next-mouse)))

;; Expand the region to different levels each time
(use-package expand-region :ensure t :defer t
  :bind (("C-=" . er/expand-region)))

;; Pep
(use-package imenu-anywhere :ensure t
  :bind (("C-c C-a" . imenu-anywhere)))

;; A filetree
(use-package neotree :ensure t :defer t
  :bind (([f8] . neotree-toggle))
  :config (setq neo-dont-be-alone t
                neo-auto-indent-point t
                neo-click-changes-root nil
                neo-show-updir-line t
                neo-theme 'ascii
                neo-mode-line-type 'none))

;; Show the amount of matches.
(use-package anzu :ensure t
  :init (global-anzu-mode +1)
  :diminish anzu-mode)

;; Save the buffer on specific actions
(use-package super-save :ensure t
  :diminish super-save-mode
  :init (progn (super-save-mode)
               (super-save-initialize)))

;; Better undo/redo system-configuration
(use-package undo-tree :ensure t :defer t
  :init (global-undo-tree-mode)
  :diminish undo-tree-mode
  :config (setq undo-tree-visualizer-timestamps t
                undo-tree-auto-save-history t
                undo-tree-history-directory-alist '(("" . "~/.local/var/emacs/undo_hist"))))


;; Better buffernames
(use-package uniquify
  :init (setq uniquify-buffer-name-style   'forward
              uniquify-after-kill-buffer-p t
              uniquify-ignore-buffers-re   "^\\*"
              uniquify-separator           t))


(use-package ivy
  :diminish ivy-mode
  :init (ivy-mode 1)
  :bind (("C-x C-f" . counsel-find-file)
         ("C-x C-b" . ivy-switch-buffer)
         ("C-c C-g " . counsel-git)
         ("C-c C-l" . counsel-locate)
         ("C-c r"   . ivy-resume)

         :map ivy-minibuffer-map
         ("RET" . ivy-alt-done)
         ("C-SPC" . ivy-alt-done)
         ("C-RET" . ivy-done))

  :config (setq ivy-use-virtual-buffers nil
                ivy-tab-space t
                ivy-format-function 'ivy-format-function-arrow
                counsel-find-file-ignore-regexp "\\`\\."
                ivy-wrap t
                projectile-completion-system 'ivy
                magit-completing-read-function 'ivy-completing-read
                ivy-count-format "(%d/%d) ")

  (use-package counsel-dash :ensure t
    :init (setq counsel-dash-common-docsets '("C" "Emacs Lisp" "Python 3" "GLib")))

  (use-package swiper :ensure t
    :bind (("C-s" . swiper)))

  (use-package ivy-youtube :ensure t :defer t
    :bind (("C-c C-y" . ivy-youtube))
    :init (setq ivy-youtube-key "AIzaSyAsk7txw-girCM1FJRNnyoOOdVbBkb0IJk"))

  (use-package ivy-gitlab :ensure t))

;; A better M-x experience
(use-package smex :ensure t :defer t
  :init  (smex-initialize)
  :config
  (defadvice smex (around space-insert-hyphen activate compile)
    (let ((ido-cannot-complete-command
           (lambda ()
             (interactive)
             (if (string=" " (this-command-keys))
                 (insert ?-)
               (funcall ,ido-cannot-complete-command)))))
      ad-do-it)))

;; Misc
(use-package window-numbering :ensure t
  :init (window-numbering-mode)
  :config (defun window-numbering-install-mode-line (&option al position) "do nothing"))

(use-package ace-window :ensure t :defer t
  :bind (("M-p" . ace-window)))

(use-package nyan-mode :ensure t :defer t
  :init (setq nyan-animate-nyancat nil
              nyan-wavy-trail t))
;;; EWWW
(use-package eww :defer t
  :init
  (setq browse-url-browser-function
        '((".*google.*maps.*" . browse-url-generic)
          ;; Github goes to firefox, but not gist
          ("http.*\/\/github.com" . browse-url-generic)
          ("groups.google.com" . browse-url-generic)
          ("docs.google.com" . browse-url-generic)
          ("melpa.org" . browse-url-generic)
          ("build.*\.elastic.co" . browse-url-generic)
          (".*-ci\.elastic.co" . browse-url-generic)
          ("internal-ci\.elastic\.co" . browse-url-generic)
          ("zendesk\.com" . browse-url-generic)
          ("salesforce\.com" . browse-url-generic)
          ("stackoverflow\.com" . browse-url-generic)
          ("apache\.org\/jira" . browse-url-generic)
          ("thepoachedegg\.net" . browse-url-generic)
          ("zoom.us" . browse-url-generic)
          ("t.co" . browse-url-generic)
          ("twitter.com" . browse-url-generic)
          ("\/\/a.co" . browse-url-generic)
          ("youtube.com" . browse-url-generic)
          ("." . eww-browse-url)))
  (setq browse-url-generic-program (executable-find "firefox-bin")
        eww-search-prefix "https://duckduckgo.com/lite/?q=")
  (add-hook 'eww-mode-hook #'toggle-word-wrap)
  (add-hook 'eww-mode-hook #'visual-line-mode))

(use-package link-hint :ensure t
  :bind ("C-c C-f" . link-hint-open-link))

(defun browse-last-url-in-brower ()
  (interactive)
  (save-excursion
    (ffap-next-url t t)))

(global-set-key (kbd "C-c C-u") 'browse-last-url-in-brower)


;; Fuck yes
(use-package elcordb  :ensure t :config (elcord-mode)
  (setq elcord-use-major-mode-as-main-icon t ))

;; Save the place you last visited the buffer
(use-package iedit :ensure t) ;; mark and edit all copies of the marked region simultaniously.
(use-package elf-mode :ensure t :defer t)
(use-package 2048-game :ensure t :defer t)
(use-package goto-chg :ensure t)
(use-package flx-ido :ensure t :config (flx-ido-mode 1))
(use-package writeroom-mode :ensure t :defer t)
