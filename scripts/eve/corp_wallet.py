#!/usr/bin/python3
import eveapi
eveapi.set_user_agent("conky_eve")

# character login
KEY_ID    = 4943609
VCODE     = "bVHfJEuT0BUMJmC8rlDpWNRlXmKhFSy4Qor5X9aJjOBc29r321DOtk90wQVqCj4Z"
char_api  = eveapi.EVEAPIConnection()
char_auth = char_api.auth(keyID=KEY_ID, vCode=VCODE)

# corporaion login
KEY_ID    = 5060362
VCODE     = "Okyfm8TB3nRoyuMiJsfcjqMCKlFEseluwIYTk0pWnGj2MiM0oLecQ2j5kW49Zzqv"
corp_api  = eveapi.EVEAPIConnection()
corp_auth = corp_api.auth(keyID=KEY_ID, vCode=VCODE)

# getting some info
sheet  = char_auth.char.CharacterSheet()
charID = sheet.characterID
corpID = sheet.corporationID
total  = 0

# getting the wallet information
for wallet in corp_auth.corp.AccountBalance().accounts:
    total += wallet.balance

print('{0:,}'.format(int(total)))
