(use-modules (ice-9 readline))
;;             (nala src)
;;             (nala colorized)
;;             (nala shell))

(activate-readline)
;;(activate-colorized)

(define (atom? x)
  (and (not (pair? x))
       (not (null? x))))

(define (add1 x)
   (+ x 1))

(define (sub1 x)
   (- x 1))

(define (zero? x)
  (eqv? x 0))

(define head car)
(define tail cdr)
(define num= =)
(define num< <)
(define num> >)
(define is-nil null?)
(define is-list list?)
