#!/bin/sh
#
# Run the bar. This is somewhat derived from Z3bra's Pop it Up tutorial
#

# how big should it be?
barw=1600 # width, is equal to the size of my screen
barh=22   # height
barx=10   # x-axis
bary=10   # a/xis/yyyy lmao

# colours
bar_bg='#ff333333'

# fonts
normal_font='Droid Sans'
icon_font='Font Awesome'
powerline_font='Source Code Pro'

# run some extra programs

cat ~/tmp/panel-fifo | ./bar_convert.sh | lemonbar -g ${barw}x${barh}x${barx}x${bary} -B $bar_bg -f "$normal_font" -f "$icon_font" -f "$powerline_font"
