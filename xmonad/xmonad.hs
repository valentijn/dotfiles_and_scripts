--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--

import           System.IO
import           XMonad
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import qualified XMonad.StackSet               as W
import           XMonad.Util.EZConfig          (additionalKeys)
import           XMonad.Util.Run               (spawnPipe)

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["Programming","Web","IM"] ++ map show [4..10]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = tiled ||| Mirror tiled ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "Gimp"       --> doFloat
    , isFullscreen --> doFullFloat]

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.
myModMask  = mod4Mask
myTerminal = "termite"

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do
  xmoproc <- spawnPipe "xmobar ~/.xmobarrc"
  xmonad $ defaultConfig
    { manageHook  = manageDocks <+> myManageHook <+> manageHook defaultConfig,
      layoutHook  = avoidStruts $ myLayout,
      logHook = dynamicLogWithPP xmobarPP
                { ppOutput = hPutStrLn xmoproc,
                  ppTitle  = xmobarColor "green" "" . shorten 20
                },
      borderWidth = 2,
      terminal    = myTerminal,
      modMask     = myModMask,
      workspaces  = myWorkspaces
    } `additionalKeys`
    [
      ((myModMask,               xK_Return), spawn myTerminal),
      ((myModMask .|. shiftMask, xK_Return), windows W.swapMaster)
    ]
