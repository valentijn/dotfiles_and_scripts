#!/bin/sh
feh --bg-scale "$@" fill

echo "$@" > ~/tmp/current_wallpaper

count=0

for x in $(colors -n 3 "$@"); do
    count=$((count + 1))

    echo $x | hex2col
    if [ $count = 1 ]; then
        bspc config normal_border_color "$x"
    elif [ $count = 2 ]; then
        bspc config focused_border_color "$x"
    elif [ $count = 3 ]; then
        bspc config urgent_border_color "$x"
    fi
done

#colors -n 16 "$@" | tee /tmp/colors | color_changer.sh | xrdb -merge
#cat /tmp/colors | hex2col
