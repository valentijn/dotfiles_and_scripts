###########
# Aliases #
###########
alias hc='herbstclient'
alias unmount='umount'          # fix a common mistake
alias pacman='pikaur'           # Arch Linux specific
alias cemacs='emacsclient -tc'  # Vital for survival
alias pregcc='gcc -E -nostdinc -std=gnu11 -ftabstop=4 -C'
alias cls='clear && ls'
alias mpct='mpc toggle'         # Random conjecture
alias mpcs='mpc status'         # Blah blah blah
alias tmux='tmux -u'		# tmux with unicode support
alias void_dwm='mg ~/opt/void-packages/srcpkgs/dwm/files/config.h; ~/opt/void-packages/xbps-src -f pkg dwm; sudo xbps-install --repository=~/opt/void-packages/hostdir/binpkgs -f dwm'
alias open='xdg-open'           # Seriously mega useful
alias nwd='nmcli dev wifi'      # shorten the NetworkManager command
alias plex='sudo /etc/init.d/plexmediaserver'
alias semacs='sudo /etc/init.d/emacs.faalentijn'
alias i3lock='i3lock -bef -i ~/usr/img/wall/gen/mountain\ shit.png'
alias scat='pygmentize -g'      # Syntax catle
alias 9999='sudo emerge -av `eix -Jc | grep 9999 | cut -d" " -f2 | tr "\n" " "`'
alias emacs="emacsclient -ca ''"
alias ncmpcpp='killall ncmpcpp 2> /dev/null; ncmpcpp 2> /dev/null' # no need to run ncmpcpp on multiple termini
alias screenshot='imlib2_grab ~/tmp/screenshot.png && uploader.sh ~/tmp/screenshot.png && rm ~/tmp/screeshot.png'
alias wallpaper='cat ~/tmp/current_wallpaper' # get the wallpaper
alias vim='emacs -nw -l ~/.evilmode.el' #  actually useful vim
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias rm='rm -f'
alias cc='gcc -Wall -Werror -g -std=gnu11 -O3'
alias gcc='gcc -Wall -Werror -g -std=gnu11 -O3'
alias python="python3"

##################
# Suffix aliases #
##################
alias -s txt=emacs
alias -s pdf=zathura
alias -s org=emacs
