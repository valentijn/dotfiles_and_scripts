#############
# Variables #
#############
#TERM=rxvt-unicode-256color

export LESS="-F -i -J -M -R -W -x 4 -X -z -4"
export EDITOR="mg"
export VISUAL="mg"
export BROWSER="firefox"
export XDG_DOWNLOAD_DIR="$HOME/tmp"
export PATH=~/.local/bin:/bin:/sbin:/usr/local/bin:~/src/wmutils/:~/bin/:$PATH
export QT_QPA_PLATFORMTHEME="qt5ct"
#export QT_STYLE_OVERRIDE=GTK+
#export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export LC_ALL=en_US.utf8
export LANG=en_US.utf8
export JAVA_HOME=/usr/lib/jvm/java-14-openjdk

# Set colors for less. Borrowed from https://wiki.archlinux.org/index.php/Color_output_in_console#less .
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

# Set the Less input preprocessor.
if type lesspipe.sh >/dev/null 2>&1; then
    export LESSOPEN='|lesspipe.sh %s'
fi

if type pygmentize >/dev/null 2>&1; then
    export LESSCOLORIZER='pygmentize'
fi
