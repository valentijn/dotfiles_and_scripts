#!/bin/sh
if [[ $1 == '-g' ]]; then
    cat bandit_passwords | grep Bandit$2 | sed s/Bandit$2:\ //g
else
    newfile=""
    number=0

    for x in $(cat bandit_passwords | sed s/Bandit[0-9][0-9]://g | sed s/Bandit[0-9]://g); do
	if [[ $number == "0" ]]; then
	    newfile="Bandit$number: $x"
	else
	    newfile="$newfile\nBandit$number: $x"
	fi
	number=$(($number+1))
    done

    echo -e $newfile > bandit_passwords
fi
