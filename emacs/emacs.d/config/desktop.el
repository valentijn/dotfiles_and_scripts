;;;
;;; This file defines a full desktop purely based on Emacs packages
;;;
(use-package exwm :ensure t :init (require 'exwm-config) (exwm-config-default))

(use-package dashboard :ensure t :defer t
  :init (dashboard-setup-startup-hook)
  :config (setq dashboard-startup-banner "/home/faalentijn/usr/img/emacs.png"
                dashboard-banner-logo-title "Vaal's Nekomacs"))

(use-package steam :ensure t :defer t
  :init (setq steam-username "faalentijn"))

(defun exwm-launch-steam-id (id)
  "Launches a steam game and sets EXWM to properly deal"
  (interactive)
  (steam-launch-id id)
  (exwm-layout-toggle-fullscreen)
  (exwm-layout-toggle-mode-line))

(use-package hydra :ensure t
  :init

  :config
  (defhydra start-menu (global-map "<f6>" :hint nil :color pink)
    "
^Emacs^                    ^Programs^
^^^^^^<==========================================>
_r_: RSS feeds (elfeed)    _c_: Crusader Kings II
_m_: Music (ampc)          _h_: Hearts of Iron IV
_e_: Email (mu4e)          _f_: Firefox
_p_: Packages              _t_: Telegram Desktop
_i_: Info                  _z_: PDF (zathura)
_s_: Shell (eshell)
_a_: Shell (ansi-term)
"
    ("r" elfeed) ("e" mu4e) ("p" package-list-packages) ("i" info) ("m" simple-mpc) ("s" eshell)
    ("c" (lambda () (interactive) (exwm-launch-steam-id 203770)))
    ("h" (lambda () (interactive) (exwm-launch-steam-id 395360)))
    ("t" (lambda () (interactive) (start-process-shell-command "Telegram" nil "telegram-desktop")))
    ("f" (lambda () (interactive) (start-process-shell-command "firefox" nil "firefox" )))
    ("z" (lambda () (interactive) (start-process-shell-command "zathura" nil "zathura")))
    ("a" (lambda () (interactive) (ansi-term "/bin/zsh")))
    ("v" split-window-below "Split vertically")
    ("h" split-window-right "Split horizontally")
    ("u" winner-undo "Undo split")
    ("x" delete-other-windows "Reset windows")
    ("q" quit-window "Exit window" :color blue)
    ("c" nil "Cancel" :stop t))

  (defhydra run-shell (global-map "C-c C-s" :exit t :hint nil)
    "
^Shells^: _p_ "
    ("p" run-python "python")
    ("e" eshell "eshell")
    ("a" ansi-term "ansi-term")
    ("s" run-geiser "scheme")
    ("c" nil "cancel")))
