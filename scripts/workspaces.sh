#!/bin/sh
# make a rofi window to switch bspwm workspaces
workspaces=""
number=0

for x in $(bspc query -D); do
	 number=$(($number + 1))

	 if [ $workspaces = ""]; then
	     workspaces="Workspace $number: $x"
	 else
	     workspaces="$workspaces\nWorkspace $number: $x"
	 fi
done

bspc desktop -f $(echo -e $workspaces | rofi -dmenu -p "Workspaces:" -i | sed s/'Workspace [0-9]:'//g)
