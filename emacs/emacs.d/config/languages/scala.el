(use-package scala-mode :ensure t :defer t
  :mode "\\.s\\(cala\\|bt\\)$")

(use-package sbt-mode :ensure t :defer t
  :commands sbt-start sbt-command
  :custom
  (sbt:default-command "testQuick")
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map))
