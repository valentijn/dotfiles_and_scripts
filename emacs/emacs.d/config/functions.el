;;;;
;;;; Small tidbits of emacs code to some stuff
;;;;

;;; Input
;; Add a ; to the end of the line.
(defun my/add-point-comma ()
  (end-of-line)
  (insert ";"))

(defun my/add-curly ()
  (end-of-line)
  (insert " {}")
  (backward-char))

(defun insert-todays-date (arg)
  "Inserts the current date into the buffer"
  (interactive "P")
  (insert (if arg (format-time-string "%Y-%m-%d")
            (format-time-string "%d-%m-%Y"))))

;; Make an int array with numbers from 1 to N
(defun int-array (number current)
  "A recursive function that inserts 'CURRENT, ' each iteration until CURRENT is equal to NUMBER"
  (insert (format "%d" current))
  (if (= number current) nil
    (progn
      (insert ", ")
      (int-array number (+ current 1)))))

(defun create-array (number)
  "Create an array from 1 to NUMBER"
  (interactive "nEnter the length of the array: ")
  (int-array number 1))

;;; Movement
;; Move to the previous buffer
(defun my/switch-to-prev-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer)))

;; Go a full page up or down in the other buffer
(defun my/other-buffer-up ()
  (interactive)
  (scroll-other-window (- 1 (window-total-size))))

(defun my/other-buffer-down ()
  (interactive)
  (scroll-other-window) (- (+ window-total-size 1) 0))

;;; Misc

;; Better relative numbers
(defun abs-rel-numbers (offset)
  (if (zerop offset)
      (format "%4d" (line-number-at-pos)) (format "%4d " (ab offset))))

;; forward and indent without moving the current line
(defun forward-and-indent ()
  (interactive)
  (forward-line)
  (indent-relative-maybe))

;; add stuff to my watchlist
(defun add-to-watchlist ()
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent)
  (insert "<li><a href=\"" (read-string "URL: ")"\">" (read-string "Name: ") "</a></li>")
  (forward-line))

;; Text expansion using C-=
(defun er/add-text-mode-expansions ()
  (make-variable-buffer-local 'er/try-expand-list)
  (setq er/try-expand-list (append er/try-expand-list '(mark-paragraph mark-page))))

;; some stuff stolen from emacs rocks
(defun untabify-buffer ()
  (interactive)
  (untabify (point-min) (point-max)))

(add-hook 'before-save-hook #'cleanup-buffer)

(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

(defun unsave-cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer.
Including indent-buffer, which should not be called automatically on save."
  (interactive)
  (untabify-buffer)
  (delete-trailing-whitespace)
  (indent-buffer))

(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer.
Including indent-buffer, which should not be called automatically on save."
  (interactive)
  (untabify-buffer)
  (delete-trailing-whitespace))

(defun goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input"
  (interactive)
  (unwind-protect
      (progn
        (linum-mode 1)
        (call-interactively 'goto-line))
    (linum-mode -1)))

(defun endless/fill-or-unfill ()
  "Like `fill-paragraph', but unfill if used twice."
  (interactive)
  (let ((fill-column
         (if (eq last-command 'endless/fill-or-unfill)
             (progn (setq this-command nil)
                    (point-max))
           fill-column)))
    (call-interactively #'fill-paragraph)))

(global-set-key [remap fill-paragraph] #'endless/fill-or-unfill)

(require 'ansi-color)
(defun endless/colorize-compilation ()
  "Colorize from `compilation-filter-start' to `point'."
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region
     compilation-filter-start (point))))

(add-hook 'compilation-filter-hook #'endless/colorize-compilation)
