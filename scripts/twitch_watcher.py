import subprocess
import time
import notify2

notify2.init("twitchwatch")
users={"jefmajor": "offline"}

while True:
    for user in users:        
        result = subprocess.Popen(["twitchnotifier", "-u", user], stdout=subprocess.PIPE).communicate()[0].decode("utf-8").rstrip()

        if result == "{} is online".format(user) and users[user] == "offline":
            n = notify2.Notification("{} is online".format(user),
                                     "foobar",
                                     "notification-message-im")
            n.show()
            users[user] = "online"
            print("test")

        elif result == "{} is offline".format(user) and users[user] == "online":
            users[user] = "offline"

    time.sleep(1200)
