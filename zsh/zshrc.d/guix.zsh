export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
export PATH="/home/faalentijn/.guix-profile/bin${PATH:+:}$PATH"
export GUILE_LOAD_PATH="/home/faalentijn/.guix-profile/share/guile/site/2.2"
export GUILE_LOAD_COMPILED_PATH="/home/faalentijn/.guix-profile/lib/guile/2.2"
