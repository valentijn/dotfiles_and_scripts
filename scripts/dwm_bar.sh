#!/bin/bash
#
# Run the bar. This is somewhat derived from Z3bra's Pop it Up tutorial
#
clock() {
    date '+%R'
}

batteryc() {
    BATC=/sys/class/power_supply/BAT0/capacity
    STAT=$(sed -n p $BATC)

    if [[ $STAT -gt 99 ]]; then
        echo "100"
    else
        echo "$STAT"
    fi
}

batterys() {
    BATC=/sys/class/power_supply/BAT0/capacity
    BATS=/sys/class/power_supply/BAT0/status

    TES=$(cat $BATS)
    STAT=$(sed -n p $BATC)
    # prepend with a '' when charging and '' when not.
    if [[ $TES == "Full" ]] || [[ $TES == "Charging" ]]; then
        echo -n ''
    elif [[ $STAT -gt "75" ]]; then
        echo -n ''
    elif [[ $STAT -gt "50" ]]; then
        echo -n ''
    elif [[ $STAT -gt "25" ]]; then
        echo -n ''
    else
        echo -n ''
    fi
}

volume() {
    VOLUME=$(ponymix get-volume)

    if [[ $VOLUME -gt "100" ]]; then
        echo "100"
    else
        echo "$VOLUME"
    fi

}

volumes() {
    VOLUME=$(ponymix get-volume)

    if ponymix is-muted; then
        echo -n ""
    else
        if [[ $VOLUME -gt "50" ]]; then
            echo -n ""
        elif [[ $VOLUME -gt "1" ]]; then
            echo -n ""
        else
            echo -n ""
        fi
    fi
}

music() {
    mpc status | grep paused &> /dev/null

    if [[ $(mpc current) == "" ]]; then
        echo "MPD paused"
    else
        mpc current
    fi
}

email() {
    EMAIL_COUNT=$(python3 ~/src/pyth/small_projects/imapmotd.py)

    if [[ $EMAIL_COUNT != 0 ]]; then
        echo "$EMAIL_COUNT"
    else
        echo "0"
    fi
}

while :; do
    xsetroot -name " $(music)   $(email)  $(volumes) $(volume)%   $(batterys) $(batteryc)%    $(clock)"
    sleep 0.1;
done
