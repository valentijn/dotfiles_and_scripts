#!/bin/sh

function run {
    if ! pgrep -f $1; then
        $@ &
    fi
}

run xrdb -merge ~/.Xresources
run compton
run setxkbmap -option 'ctrl:nocaps'
