;;
;; Parses the json from Google Play Music Desktop Player
;;

(defvar *json-path* "~/.config/Google Play Music Desktop Player/json_store/playback.json"
  "Path of the json object")
(defvar *song-name* nil "Name of the song currently playing")
(defvar *artist* nil "Name of the artist")
(defvar *album* nil "Name of the album")
(defvar *shuffle* nil "Status of the shuffle function (boolean)")
(defvar *repeat* nil "Whether it is repeating or not (boolean)")
(defvar *playing* nil "Status if it is playing or not (boolean)")
(defvar *time-total* nil "Total length of the song in seconds")
(defvar *time-played* nil "Current spot of the song in seconds")

(load "~/quicklisp/setup.lisp")
(ql:quickload "cl-json")

(defun seconds-to-minutes (milliseconds)
  (let* ((seconds (truncate (/ milliseconds 1000)))
	 (minutes (truncate (/ seconds 60)))
	 (seconds (- seconds (* minutes 60))))
    (format nil "~D:~2,'0D" minutes seconds)))

(defun get-information ()
  (let* ((output (cl-json:decode-json (open *json-path*)))
	 (song-info (cdr (assoc :SONG output)))
	 (time (cdr (assoc :TIME output))))

    (setf *song-name* (cdr (assoc :TITLE song-info))
	  *artist* (cdr (assoc :ARTIST song-info))
	  *album* (cdr (assoc :ALBUM song-info))
	  *shuffle* (equal (cdr (assoc :SHUFFLE output)) "NO_SHUFFLE")
	  *repeat* (equal (cdr (assoc :REPEAT output)) "SINGLE_REPEAT")
	  *playing* (cdr (assoc :PLAYING output))

	  *time-total* (seconds-to-minutes (cdr (assoc :TOTAL time)))
	  *time-played* (seconds-to-minutes (cdr (assoc :CURRENT time))))))

*time-played*
(defun write-information ()
  (format nil "~A (~A) by ~A at ~A/~A" *song-name* *album* *artist* *time-played* *time-total*))

(defun run-function ()
  (get-information)
  (write-information))
