((magit-log
  ("-n256" "--graph" "--decorate"))
 (magit-rebase nil
			   ("--preserve-merges" "--interactive")))
