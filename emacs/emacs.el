;;;
;;; Emacs configuration files.
;;; The Split File Edition.
;;;

;;; Code:

;; Set the package sources
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(setq package-check-signature nil)
(setq package-archives
      '(("GNU"   . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://melpa.org/packages/")))

;; initialize the package manager
(package-initialize)

(unless (require 'use-package nil 'noerror)
  (package-refresh-contents)
  (package-install 'use-package)
  (require 'use-package nil 'noerror))

(use-package paradox :ensure t :defer t)
(use-package package-utils :ensure t :defer t)
(use-package try :ensure t :defer t)
;;(use-package exwm :ensure t :init (require 'exwm-config) (exwm-config-default))

;;(load-file "~/.emacs.d/config/desktop.el")
(load-file "~/.emacs.d/config/packages.el")
(load-file "~/.emacs.d/config/mu4e_config.el")
(load-file "~/.emacs.d/config/org_config.el")
(load-file "~/.emacs.d/config/rss.el")
(load-file "~/.emacs.d/config/functions.el")
(load-file "~/.emacs.d/config/keybinds.el")
(load-file "~/.emacs.d/config/modes.el")
;; (load-file "~/.emacs.d/config/irc.el")
(load-file "~/.emacs.d/config/languages/init.el") ;; replacement for modes.
(load-file "~/.emacs.d/config/configuration.el")

;; Force load the theme in daemon mode.
(defun init-theme (frame)
  (select-frame frame)
  (load-file "~/.emacs.d/config/themes.el"))


(if (daemonp)
    (add-hook 'after-make-frame-functions #'init-theme)
  (load-file "~/.emacs.d/config/themes.el"))

;; Stuff added by the customizer and hence I don't want to touch.ki
;;
;; custom-set-variables was added by Custom.
;; If you edit it by hand, you could mess it up, so be careful.
;; Your init file should contain only one such instance.
;; If there is more than one, they won't work right.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(arduino-executable "/home/valentijn/Downloads/arduino-1.8.13/arduino")
 '(asm-comment-char 35)
 '(custom-safe-themes
   '("423435c7b0e6c0942f16519fa9e17793da940184a50201a4d932eafe4c94c92d" "f2b83b9388b1a57f6286153130ee704243870d40ae9ec931d0a1798a5a916e76" "f30aded97e67a487d30f38a1ac48eddb49fdb06ac01ebeaff39439997cbdd869" "82d2cac368ccdec2fcc7573f24c3f79654b78bf133096f9b40c20d97ec1d8016" "bb749a38c5cb7d13b60fa7fc40db7eced3d00aa93654d150b9627cabd2d9b361" "c44056a7a2426e1229749e82508099ba427dde80290da16a409d5d692718aa11" "8bb8a5b27776c39b3c7bf9da1e711ac794e4dc9d43e32a075d8aa72d6b5b3f59" "06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d" "06b2849748590f7f991bf0aaaea96611bb3a6982cad8b1e3fc707055b96d64ca" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" default))
 '(display-time-24hr-format t)
 '(display-time-day-and-date nil)
 '(doom-dracula-brighter-comments t)
 '(doom-one-comment-bg t)
 '(erc-modules
   '(autojoin button completion fill irccontrols list match menu move-to-prompt netsplit networks noncommands readonly ring scrolltobottom smiley stamp spelling track))
 '(geiser-guile-load-path '("~/src/lisp/guile"))
 '(lsp-enable-on-type-formatting nil)
 '(lsp-ui-doc-use-childframe t)
 '(magit-commit-arguments '("--verbose" "--gpg-sign=3472D06EC721BD07") t)
 '(midnight-mode t)
 '(org-agenda-files
   '("~/pvda/wbs/de_nederlandse_droom.org" "~/usr/doc/opdr/ui/verslag.org"))
 '(package-selected-packages
   '(go-mode yaml company-lsp lsp-ui sbt-mode lsp-metals scala-mode emacs-ccls sml-mode counsel-spotify frameshot elcord rainbow-identifiers rainbow-identifiers-mode lsp-ivy groovy-mode meghanada gradle-mode xref-js2 js2-refactor js2-mode web-mode php-mode yaml-mode nyan-mode diminish solaire-mode yasnippet-snippets undo-tree virtualenvwrapper all-the-icons spinner ob-sml rainbow-delimiters geiser paredit x86-lookup lua-mode auctex-lua quickrun nim-mode eglot guix org-ref smartparens company-quickhelp lsp-python steam package-utils try paradox eshell-prompt-extras which-key multi-compile beginend begined dashboard hydra netherlands-holidays link-hint gnome-c-style telephone-line doom-themes quack sly-repl-ansi-colors sly-quicklisp sly-named-readtable sly-macrostep sly-company sly disaster ace-window flycheck-plantuml plantuml-mode git-gutter-fringe achievements sourcerer-theme company-web nasm-mode password-store counsel-dash arduino-mode color-theme-sanityinc-tomorrow ivy-gitlab ivy-youtube mu4e-alert use-package eshell-git-prompt shell-pop better-shell latex-math-preview sourceror-theme elfeed-org ivy elf-mode org-bullets let-alist))
 '(paradox-github-token t)
 '(pdf-view-midnight-colors '("#DCDCCC" . "#383838"))
 '(safe-local-variable-values
   '((eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (set
              (make-local-variable 'compile-command)
              (concat "cd "
                      (concat root "src")
                      " && make test"))
             (let
                 ((map
                   (make-sparse-keymap)))
               (set-keymap-parent map
                                  (current-local-map))
               (use-local-map map)
               (local-set-key
                [f5]
                'compile)
               (local-set-key
                [f6]
                'co/gdb)
               (defun co/gdb nil
                 (interactive)
                 (async-shell-command
                  (concat "cd "
                          (concat
                           (projectile-project-root)
                           "src")
                          " && " "make debug")
                  nil 0)
                 (gdb gud-gdb-command-name)))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (set
              (make-local-variable 'compile-command)
              (concat "cd "
                      (concat root "src")
                      " && make test"))
             (use-local-map
              (copy-keymap
               (current-local-map)))
             (local-set-key
              [f5]
              'compile)
             (local-set-key
              [f6]
              'co/gdb)
             (defun co/gdb nil
               (interactive)
               (async-shell-command
                (concat "cd "
                        (concat
                         (projectile-project-root)
                         "src")
                        " && " "make debug")
                nil 0)
               (gdb gud-gdb-command-name))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (setq-local realgud:gdb-command-name command)
             (setq-local compile-command
                         (concat "cd "
                                 (concat
                                  (vc-root-dir)
                                  "src")
                                 " && make test"))
             (defun course_os/gdb nil
               (interactive)
               (async-shell-command
                (concat "cd "
                        (concat
                         (vc-root-dir)
                         "src")
                        " && " "make debug")
                nil 0)
               (gdb gud-gdb-command-name))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (setq-local realgud:gdb-command-name command)
             (defun course_os/gdb nil
               (interactive)
               (async-shell-command
                (concat "cd "
                        (concat
                         (vc-root-dir)
                         "src")
                        " && " "make debug")
                nil 0)
               (gdb gud-gdb-command-name))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (setq-local realgud:gdb-command-name command)
             (defun course_os/gdb nil
               (interactive)
               (async-shell-command
                (concat "cd "
                        (concat
                         (vc-root-dir)
                         "src")
                        " && " "make debug")
                nil 0)
               (gdb))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (setq-local realgud:gdb-command-name command)
             (defun course_os/gdb nil
               (interactive)
               (async-shell-command
                (concat "cd "
                        (concat
                         (vc-root-dir)
                         "src")
                        " && " "make debug")
                nil 0)
               (realgud:gdb))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (setq-local realgud:gdb-command-name command)
             (defun course_os/gdb nil
               (interactive)
               (setq-local default-directory
                           (concat
                            (file-name-directory buffer-file-name)
                            "/src"))
               (async-shell-command "make debug" nil 0)
               (realgud:gdb))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)
             (setq-local realgud:gdb-command-name command)
             (defcustom gdb-command gdb "Command used to start gdb" :type
               '(function)
               :group 'courseos :local t)
             (custom-variable-p)
             (add-hook realgud:gdb)
             (defun course_os/gdb nil
               (interactive)
               (setq-local default-directory
                           (concat
                            (file-name-directory buffer-file-name)
                            "/src"))
               (async-shell-command "make debug" nil 0)
               (if
                   (gdb-command))
               (realgud:gdb))))
     (eval let
           ((root
             (projectile-project-root)))
           (let
               ((command
                 (concat "arm-none-eabi-gdb -i=mi -ex \"target remote localhost:1234\" -ex \"symbol-file " root "src/kernel/build/kernel.sym\"")))
             (setq-local gud-gud-gdb-command-name command)
             (setq-local gud-gdb-command-name command)))
     (eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")
     (bug-reference-bug-regexp . "<https?://\\(debbugs\\|bugs\\)\\.gnu\\.org/\\([0-9]+\\)>")))
 '(solaire-global-mode t)
 '(tab-width 4))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Info-quoted ((t (:slant italic :weight light :foundry "source" :family "code"))))
 '(font-lock-string-face ((t (:foreground "#98be65"))))
 '(hl-line ((t (:weight bold)))))

(put 'narrow-to-region 'disabled nil)
