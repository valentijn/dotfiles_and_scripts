#
# This is my library of useful functions I wrote for myself
#


def simplify(x, y):
    """
    Simplifies a x/100 equation.
    Expects x > 0 and y > 0
    """
    # Check if the numerator is larger than the denominator
    if x // y >= 1:
        result = "{} ".format(x // y)
        # take the rest to simplify further.
        x = x % y
    else:
        result = ""

    # Check all numbers between x and 1 in decreasing order.
    # This simplifies the function and reduces computation time.
    for i in range(x // 2, 1, -1):
        # Check if dividing x or y by i will produce a floating point.
        # NOT compatible with Python 2.x. Use float(x) / float(y) instead!
        if (x / i % 1 == 0) and (y / i % 1 == 0):
            # Return the answer in an easy to read format.
            # By the way, // is for integer division.
            # We do not lose information by using // due to the nature of the
            # problem
            return result + "{0}/{1}".format((x // i), (y // i))

    return result + "{0}/{1}".format(x, y)
