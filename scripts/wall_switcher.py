import datetime
import getpass
import glob
import random
import time
from subprocess import call

while True:
    hour = ''.join(str(datetime.datetime.now()).split()[1:])[:2]
    nsfw = False
    path = "/home/{}/usr/img/wall/".format(getpass.getuser())

    if int(hour) > 16 and int(hour) < 23:
        if nsfw == True:
            #            path = glob.glob(path + "anime/pics/*") + glob.glob(path + "anime/ntfs/*") + glob.glob(path + "gen/*")
            picture = random.randint(0, len(path) - 1)
            call(["foobar.sh", path[picture]])
        else:
            path = glob.glob(path + "anime/pics/eru*") + glob.glob(path + "anime/pics/chitanda*")
            #            path = glob.glob(path + "anime/pics/*") + glob.glob(path + "gen/*")
            picture = random.randint(0, len(path) - 1)
            call(["foobar.sh", path[picture]])
    elif int(hour) >= 23 or int(hour) < 7:
        if nsfw == True:
            path = glob.glob(path + "../ntfs/wall/*") + glob.glob(path + "anime/ntfs/*")
            picture = random.randint(0, len(path) - 1)
            call(["foobar.sh", path[picture]])
        else:
            path = glob.glob(path + "pony/*") + glob.glob(path + "anime/pics/*") + glob.glob(path + "anime/ntfs/*") + glob.glob(path + "gen/*")
            picture = random.randint(0, len(path) - 1)
            print(path[picture])
            call(["foobar.sh", path[picture]])
    else:
        path = glob.glob(path + "comp/*") + glob.glob(path + "gen/*") + glob.glob(path + "games/*") + glob.glob(path + "starwars/*")
        picture = random.randint(0, len(path) - 1)
        call(["foobar.sh", path[picture]])

    time.sleep(600)
