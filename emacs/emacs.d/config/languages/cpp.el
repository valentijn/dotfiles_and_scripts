(defun my/c++-config ()
  (irony-eldoc)
  (semantic-mode)
  (add-to-list 'company-backends '(company-irony
                                   :with company-irony-c-headers
                                   :with company-yasnippet))
  (define-key irony-mode-map [remap completion-at-point] 'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol] 'irony-completion-at-point-async)
  (irony-cdb-autosetup-compile-options)
  (add-to-list 'auto-mode-alist '("/usr/include/qt5" . c++-mode))
  (semantic-add-system-include "/usr/include/qt5" 'c++-mode)
  (add-to-list 'semantic-lex-c-preprocessor-symbol-file "/usr/include/qt5/Qt/qconfig.h")
  (add-to-list 'semantic-lex-c-preprocessor-symbol-file "/usr/include/qt5/Qt/qtconfig-dist.h")

  (c-set-offset 'case-label '+)
  (c-set-offset 'cpp-macro 0)
  (c-set-offset 'cpp-macro-cont 1)

  (push "/usr/include/qt5" flycheck-clang-include-path)
  (push "/usr/include/qt5" flycheck-gcc-include-path)

  (setq-local flycheck-gcc-language-standard "c++17")
  (setq-local sp-escape-quotes-after-insert nil))

(add-hook 'c++-mode-hook #'my/c++-config)
