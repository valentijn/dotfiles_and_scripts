;;;
;;; This meant for the configuration of bigger emacs packages like gnus, C mode or eshell
;;;

;; colorize
(require 'ansi-color)
(defun endless/colorize-compilation ()
  "Colorize from `compilation-filter-start' to `point'."
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region
     compilation-filter-start (point))))

(add-hook 'compilation-filter-hook #'endless/colorize-compilation)
;; Quickies
(add-hook 'arduino-mode-hook 'irony-mode)
(add-hook 'calendar-mode-hook #'diary-mark-entries)

;; Web mode
(defun web-configuration ()
  (set (make-local-variable 'company-backends) '(company-web-html :with company-yasnippet))
  (local-key-binding (kbd "C-x M-m p") 'php-mode)
  (local-key-binding (kbd "C-x M-m h") 'html-mode)
  (remove-hook 'before-save-hook 'cleanup-buffer))

(add-hook 'html-mode-hook #'web-configuration)
(add-hook 'php-mode-hook #'web-configuration)

;; Stuff
(add-hook 'isearch-mode-end-hook #'endless/goto-match-beginning)

(defun endless/goto-match-beginning ()
  "Go to the start of current isearch match.
Use in `isearch-mode-end-hook'."
  (when (and isearch-forward
             (number-or-marker-p isearch-other-end)
             (not mark-active)
             (not isearch-mode-end-hook-quit))
    (goto-char isearch-other-end)))

;; Emacs Shell mode
(defun eshell-config ()
  (use-package virtualenvwrapper
    :ensure t
    :config
    (venv-initialize-interactive-shells)
    (venv-initialize-eshell))

  (use-package better-shell
    :ensure t
    :bind (("C-'" . better-shell-shell)
           ("C-;" . better-shell-remote-open)))

  (use-package eshell-git-prompt :ensure t)
  (use-package shell-pop :ensure t)
  (use-package eshell-prompt-extras :ensure t :defer t)

  (with-eval-after-load "esh-opt"
    (require 'virtualenvwrapper)
    (venv-initialize-eshell)
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-highlight-prompt nil
          eshell-prompt-function 'epe-theme-lambda))

  (require 'eshell)
  (require 'em-smart)                   ;

  (setenv "PAGER" "cat")
  (setq eshell-where-to-jump          'begin
        eshell-review-quick-commands   nil
        eshell-smart-space-goes-to-end t
        eshell-list-files-after-cd     t
        eshell-prefer-lisp-functions   nil)

  (setq eshell-visual-commands '("vim" "less" "mutt" "more" "newsbeuter" "podbeuter"
                                 "ncmpcpp" "cgdb" "tmux")))

(add-hook 'eshell-mode-hook #'eshell-config)

;; Message mode
(add-hook 'message-mode-hook
          (lambda ()
            (setq fill-column 72)
            (turn-on-auto-fill)))

(add-hook 'text-mode-hook (lambda () (auto-fill-mode 1)))
