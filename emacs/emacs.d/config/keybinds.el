;;;
;;; Vaal's Emacs Keybinds.
;;;

;; Massively simplify my keybinds setup.
(use-package crux :ensure t)
(use-package bind-key :ensure t)
(use-package free-keys :ensure t)

(dotimes (n 10) (global-unset-key (kbd (format "C-%d" n))))

;; Go up and down in a buffer
(bind-keys
 ("C-1 d" . my/other-buffer-down)
 ("C-1 C-u" . my/other-buffer-up))

(bind-keys
 :map comint-mode-map
 ("SPC" . comint-magic-space))

;; Run a few of my favourite apps. If it has a * it will override all attemps to rebind
(bind-keys
 :prefix-map launcher
 :prefix "C-1"
 ("m"   . ampc)
 ("C-m" . ampc)
 ("b"   . eww)
 ("C-b" . eww)
 ("f"   . my/switch-to-prev-buffer)
 ("C-f" . my/switch-to-prev-buffer)
 ("e"   . eval-buffer)
 ("C-e" . eval-buffer)
 ("p"   . package-install)
 ("C-p" . package-list-packages)

 ("b"   . browse-url)
 ("C-b" . browse-url)
 ("s"   . eshell)
 ("C-s" . eshell)
 ("q"   . kill-this-buffer)
 ("C-q" . kill-this-buffer))

;; Some modes have an electic indent mode which doesn't do exactly what I want.
;; And, you know, fuck that shit.
(bind-keys
 ("C-<tab>" . indent-relative)
 ("RET"     . newline-and-indent)

 ;; A neat function when you're working with snippets and the like.
 ("C-RET"   . forward-and-indent)
 ("C-x C-j" . forward-line))

;; Sometimes useful but vaguely useless
(bind-keys
 :map prog-mode-map
 ("C-;"     . my/add-point-comma)
 ("C-{"     . my/add-urly))

;; Unbind the normal use of mouse in progmode in favour of multiple cursors
(bind-key "M-<down-mouse-1>" 'mc/add-cursor-on-click prog-mode-map)

;; Rebind default keys
(bind-keys*
 ("C-x C-b" . ibuffer)                ; a better buffer switcher
 ("C-x C-z" . repeat)                 ; repeat command
 ("C-x C-d" . backward-kill-word)     ; kill word
 ("M-x"     . smex)                   ; a better M-x using IDO
 ("C-c x"   . smex-major-mode-commands)
 ("C-c M-x" . execute-extended-command))

;; Not sure how to do this with bind-key

;; Info mode map keys
(bind-keys
 :map Info-mode-map
 ("k" . next-line)
 ("K" . next-line)
 ("j" . previous-line)
 ("J" . previous-line)
 ("n"       . next-line)
 ("p"       . previous-line)
 ("N"       . Info-next)
 ("P"       . Info-prev))

;; CRUX keybinds which I thought deserved to be here
(bind-keys
 :prefix-map crux-prefix
 :prefix "C-c"
 ("C-a" . crux-smart-open-line-above)
 ("C-b" . crux-smart-open-line)
 ("C-o" . crux-open-with)
 ("M-u" . crux-view-url)
 ("M-e" . crux-eval-and-replace)
 ("M-d" . crux-delete-file-and-buffer)
 ("M-k" . crux-kill-other-buffers)
 ("C-j" . crux-top-join-line)
 ("C-i" . crux-ispell-word-then-abbrev)
 ("C-r" . crux-rename-file-and-buffer))

(bind-keys*
 ;; Defaults I agree with
 ([remap move-beginning-of-line] . crux-move-beginning-of-line)
 ([(shift return)] . crux-smart-open-line)
 ([remap kill-whole-line] . crux-kill-whole-line)
 ([remap goto-line] . goto-line-with-feedback) ; better goto line
 ([remap electric-newline-and-maybe-indent] . newline-and-indent))
