zstyle ':completion:*' menu select # mouse driven completion
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

##############
# Completion #
##############
zstyle ':completion:*' rehash true
autoload -U run-help
autoload run-help-git
autoload run-help-svn
autoload run-help-svk
alias help=run-help
