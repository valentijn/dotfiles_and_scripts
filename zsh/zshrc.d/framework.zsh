##################
# zsh framework  #
##################
source ~/.zplug/init.zsh

# zplug installs missing packages
if ! zplug check; then
    zplug install
fi

# zsh-syntax-highlighting must be loaded after executing
# compinit command and sourcing other plugins
zplug "zplug/zplug"
zplug "zsh-users/zsh-syntax-highlighting", defer:3

# zplug requires double commas.
zplug "zsh-users/zsh-history-substring-search" # substring search
zplug "zsh-users/zsh-completions"              # completion
zplug "djui/alias-tips"                        # warn if an alias exists for a command
zplug "rimraf/k"                               # A better ls
zplug "Tarrasch/zsh-bd"                        # Time back cd
zplug "chrissicool/zsh-256color"               # Use 256 colours
zplug "hcgraf/zsh-sudo"                        # double press esc to run as sudo
zplug "MichaelAquilina/zsh-emojis"             # Every day we stray from god's light
zplug "RobSis/zsh-completion-generator"        # Generate completions
zplug "zsh-users/zsh-completions"              # More completions
zplug "gradle/gradle-completion"
zplug "zsh-users/zsh-autosuggestions", defer:3 # auto suggestions should be loaded last

# themes
#zplug "sindresorhus/pure"
#zplug "caiogondim/bullet-train-oh-my-zsh-theme", rename-to:bullet-train
zplug "subnixr/minimal"

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Source plugins and add commands to path
zplug load
