;;;
;;; commands.lisp
;;; Define commands
;;;

;; prompt the user for an interactive command. The first arg is an
;; Fetch and echo a fortune.
(defcommand fortune () ()
  "Run fortune and print it to the screen"
  (if (pathname-is-executable-p "/usr/bin/fortune")
      (echo (run-shell-command "fortune" t))
      (echo "Fortune is not installed on your system")))

;; Ask to enable the modeline or not
(defcommand modeline-check (enabled) ((:y-or-n "Do you want to enable the modeline? "))
  (when enabled
    (echo "test")
    (load "~/.config/stumpwm/modeline.lisp")))

;; redefine run-shell-command for 'zsh', change :shell "", and fix a typo.
(defcommand run-shell-command (cmd &optional collect-output-p)
    ((:shell "execute: "))
  "Run the specified shell command. If @var{collect-output-p} is @code{T}
            then run the command synchronously and collect the output."
  (if collect-output-p
      (run-prog-collect-output *shell-program* "-c" cmd)
      (run-prog *shell-program* :args (list "-c" cmd) :wait nil)))
(defcommand-alias exec run-shell-command)
;;  (echo enabled))
