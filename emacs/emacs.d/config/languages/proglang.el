;;;
;;; Configuration for the proglang MOOC course
;;;

;; Make spacing great again
(defun insert-var ()
  (interactive)
  (insert " : "))

;; (req-package company-sml
;; :loader:elpa
;; :require sml-mode
                                        ;  :config (add-hook 'sml-mode-hook 'company-sml-setup))

(defun my/run-sml ()
  (interactive)
  (run-sml "sml" ""))



(use-package sml-mode
  :ensure t
  :defer t
  :init (add-hook 'sml-mode-hook '(lambda () (abbrev-mode -1) (aggressive-indent-mode)))

  :bind (:map sml-mode-map
              ("M-'" . insert-var)
              ("C-x c" . my/run-sml))

  :config (sp-local-pair 'sml-mode "(* " " *)")

  ;; Use C-h v to read what these actually do
  (setq sml-electric-semi-mode nil))
