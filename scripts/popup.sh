#!/bin/zsh

# how long should the popup remain?
duration=3

# geometry
barx=10
bary=10
barw=120
barh=20

# colours
bar_bg='#ff333333'
bar_fg='#ffffffff'

# font used
bar_font='Terminus'

# compute all this info
baropt=" -g ${barw}x${barh}+${barx}+${bary} -B ${bar_bg} -f ${bar_font}"

print $baropt
# Create the popup and make it live for 3 seconds
(echo "%{c} $@"; sleep ${duration}) | lemonbar -g ${barw}x${barh}+${barx}+${bary} -B ${bar_bg} -f ${bar_font}
