# A simple HTTP webserver for testing purposes.

from http.server import HTTPServer, BaseHTTPRequestHandler


class HTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        # Successful response
        self.send_response(200)

        # The content of the file is HTML (mime)
        self.send_header("Content-type", "text/html")

        # Allow access to the website using XHR requests
        self.send_header("Access-Control-Allow-Origin", "*")

        # Stop sending headers
        self.end_headers()

        # Write data to the file
        self.wfile.write(bytes("<html><head></head><body><h1>Foo</h1></body></html>",
                               "utf-8"))

    def do_POST(self):
        pass


def run(server_class=HTTPServer, handler_class=HTTPRequestHandler):
    # Run the server using our custom HTTP Request Handler
    server_address = ('127.0.0.1', 1500)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

run()
