#!/bin/sh
#
# surf the web using the magic of roofies
#

# check if the url for storing the search history exists and if not create it
# required for -dmenu to work with rofi
if [[ ! -f ~/.cache/urlhistory ]]; then
    touch ~/.cache/urlhistory
    echo "https://duckduckgo.com/" > ~/.cache/urlhistory
fi

# get the url history, start rofi to pick one and remove www. to prevent duplicates
url=$(cat ~/.cache/urlhistory | rofi -p "URL: " -i -dmenu -l 5 | sed s/'www.'//g)
size=${#url} # get the size

# check if the last letter of the url is / if not add it. It's to
# prevent duplicates in urlhistory
if [[ ! $(echo "$url" | cut -c $size-$size) == "/" ]]; then
    url="$url/"
fi

# check what protocol it uses and if it's http replace it
# for https in the history file to prevent duplicates
if [[ $(echo "$url" | cut -c 1-5) == "https" ]]; then
    # grep the file for the url
    grep=$(grep "$url" ~/.cache/urlhistory)

    # if the url is not in the file put it into the file
    if [[ "$grep" != "$url" ]]; then
	echo "$url" >> ~/.cache/urlhistory
    fi
    
elif [[ $(echo "$url" | cut -c 1-4) == "http" ]]; then
    url=$(echo "$url" | sed s/'http'/'https'/g)
    grep=$(grep "$url" ~/.cache/urlhistory)

    if [[ "$grep" != "$url" ]]; then
	echo $url >> ~/.cache/urlhistory
    fi

# if there is neither use it as a way to search duckduckgo
else
    arg=""
    for x in $url; do
	if [[ $arg == " " ]]; then
	    arg=$x
	else
	    arg="$arg%20$x"
	fi

    done

    arg=$(echo "$arg" | sed s/'\/'//g)
    url="https://duckduckgo.com/?q=$arg"
fi

# open it using your default browser. I recommend surf
xdg-open "$url"
