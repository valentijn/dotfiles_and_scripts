#!/bin/sh
#
# This script allows you to interactively launch an environment of your choice.
#


echo -e "1) Dynamic Window Manager\n2) Binary Space Partitioning Window Manager\n3) XFCE\n4) wmutil\n5) Herbstluft\n6) Stumpwm\n7) Emacs"
printf "Please pick a window manager or desktop environment you want to launch: "; read -rn1 ans

case $ans in
    1)
        echo "exec dwm"
        ;;
    2)
        echo -e "exec sxhkd &\nexec bspwm"
        ;;
    3)
        echo "exec startxfce4"
        ;;
    4)
        echo -e "cd ~/src/wmutils; sxhkd -c sxhkdrc &\nexec urxvt"
        ;;
    5)
        echo "exec herbstluftwm"
        ;;
    6)
        echo "exec stumpwm"
        ;;
    7)
        echo "exec emacs"
        ;;
esac > ~/.wm-info.sh
