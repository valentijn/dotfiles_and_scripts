##################
# history config #
##################
SAVEHIST=1000
HISTSIZE=1000
HISTFILE=~/.zhistory
setopt append_history inc_append_history share_history extended_history histignorealldups
setopt hist_ignore_dups hist_ignore_all_dups hist_find_no_dups hist_save_no_dups
setopt hist_reduce_blanks no_hist_beep hist_ignore_space hist_no_store hist_no_functions


########
# Misc #
########
setopt no_beep       # disable beeping
setopt auto_cd       # if it's not a command but a dir cd into it
setopt cd_able_vars  # similar to auto_cd but for variables
setopt multios       # Something to with tee or something? It looks important
setopt correct       # Correct words
setopt notify        # Long live lenin
setopt no_hup        # don't close programs running in the terminal
setopt autocd        # Don't require cd before a directory
setopt COMPLETE_ALIASES
setopt autopushd
setopt pushdignoredups

###########:
# Modules #
###########
autoload -U tetris
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'
