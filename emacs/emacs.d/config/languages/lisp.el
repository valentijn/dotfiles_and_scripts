;;;
;;; Configuration file for most lispsi (?(?!))
;;;

;; Emacs Lisp Configuration
(add-hook 'emacs-lisp-mode-hook #'(lambda () (flycheck-mode 0)))

;; Common Lisp Configuration
(use-package sly :ensure t :defer t :disabled t
  :init (setq inferior-lisp-program "sbcl"
              sly-autodoc-use-multiline-p t)
  :config (progn
            (sly-autodoc-mode 1)
            (use-package sly-company
              :ensure t
              :init
              (add-hook 'sly-mode-hook 'sly-company-mode)
              (add-to-list 'company-backends 'sly-company))

            (use-package sly-macrostep :ensure t)
            (use-package sly-named-readtable :ensure t)
            (use-package sly-quicklisp :ensure t)
            (use-package sly-repl-ansi-colors :ensure t
              :init (push 'sly-repl-ansi-color sly-contribs)
              :config (sly-setup))))


;; Scheme Configuration
(use-package racket-mode :ensure t :defer t)
(use-package quack :ensure t :defer t)
(use-package paredit :ensure t)

;; A better version of geiser-doc-look-up-manual which changes to another window
(defun my/geiser-doc-look-up-manual ()
  (interactive)
  (let ((buf (current-buffer)))
    (geiser-doc-look-up-manual)
    (switch-to-buffer buf)
    (other-window 1)
    (info)
    (pop-to-buffer buf)))

;; A package for writing scheme
(use-package geiser :ensure t :defer t
  :bind (("C-c C-p" . geiser-doc-symbol-at-point)
         ("C-c C-d C-i" . my/geiser-doc-look-up-manual))
  :init (setq geiser-default-implementation 'guile
              geiser-active-implementations '(guile)
              geiser-repl-forget-old-error-p nil
              geiser-repl-history-filename "~/.emacs.d/geiser-history")
  :config (use-package geiser-guile))

;; Give different colours to the different levels of parenthesis.
(use-package rainbow-delimiters :ensure t
  :init (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(add-hook 'scheme-mode-hook
          (lambda ()
            (push '("lambda" . ?λ) prettify-symbols-alist)
            (push '("lambda*" . (?λ (Br . Bl) ?*)) prettify-symbols-alist)
            (push '("lambda* () " . (? (Br . Bl) ?* (Br . Bl) ? )) prettify-symbols-alist)
            (push '("<=" . ?≤) prettify-symbols-alist)
            (push '(">=" . ?≥) prettify-symbols-alist)
            (push '("->" . ?→) prettify-symbols-alist)
            (push '("map" . ?Σ) prettify-symbols-alist)
            (push '("for-each" . ?∀) prettify-symbols-alist)
            (push '("not" . ?¬) prettify-symbols-alist)
            (push '("and" . ?&) prettify-symbols-alist)
            (push '("or" . ?|) prettify-symbols-alist)
            (push '("lambda () " . (? (Br . Bl) ? )) prettify-symbols-alist)))
