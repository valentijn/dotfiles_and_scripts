(use-package erc :defer t
  :init
  (use-package erc-match :defer t)
  (use-package erc-nicklist :defer t)
  (defun my/start-irc ()
    (interactive)
    (if (get-buffer "irc.freenode.net:6667")
        (erc-track-switch-buffer 1)
      (when (erc-tls :server "irc.freenode.net" :port 6697 :nick "Faalentijn")
        (erc-tls :server "irc.oftc.net" :port 6697 :nick "Valentijn"))))

  :config
  (erc-autojoin-mode t)

  (setq erc-notice-in-minibuffer-flag t
        erc-track-position-in-mode-line t
        erc-timestamp-format "[%H:%M] "
        erc-keywords '("Faalentijn" "faalentijn" "Valentijn")
        erc-fill-prefix "     "
        erc-nick "ValentijnvdBeek"
        erc-user-full-name "Valentijn"
        erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT" "MODE"
                                  "324" "329" "332" "333" "353" "477")
        erc-hide-list '("JOIN" "PART" "QUIT" "NICK" "MODE")
        erc-autojoin-channels-alist '((".*\\.oftc.net" "#clay")
                                      (".*\\.freenode.net" "#guile"))))
