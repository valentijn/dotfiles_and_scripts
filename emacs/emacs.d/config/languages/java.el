(use-package meghanada :defer t :ensure t
  :init
  (add-hook 'java-mode-hook
            (lambda ()
              (set (make-local-variable 'compile-command)
                   (concat "javac " buffer-file-name " && java"
                           (file-name-sans-extension buffer-file-name)))

              (meghanada-mode t)
              (highlight-symbols-mode t)
              (setq c-basic-offset 4)
              (add-hook 'before-save-hook 'meghanada-code-beautify-before-save))))

(use-package gradle-mode :defer t :ensure t
  :init
  (add-hook 'java-mode-hook (lambda () (gradle-mode 1))))
