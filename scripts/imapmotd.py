#!/usr/bin/python2
import imaplib
import time

def read():
    # Login into the mailbox
    imap = imaplib.IMAP4_SSL("imap.openmailbox.org", 993)
    imap.login("foo", "foo")
    imap.select()
        
    # Check the amount of unread emails I have.
    unreadt = imap.search(None, 'UnSeen')
    unreadto = unreadt[1]
    unreadl = unreadto[0]
    return len(unreadl.split())

def clear():
    # Login into the mailbox
    imap = imaplib.IMAP4_SSL("posteo.net", 993)
    imap.login("foo", "bar")
    imap.select()
        
    # Check the amount of unread email I have
    unreadt = imap.search(None, 'UnSeen')
    unreadto = unreadt[1]
    unreadl = unreadto[0]
    return len(unreadl.split())
    
print(read() + clear())



