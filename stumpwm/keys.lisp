;;;
;;; keys.lisp
;;; Keybinds for stumpwm also currently used for function definitions (might be moved out)
;;;

(define-key *root-map* (kbd "C-i") "show-window-properties")
(define-key *root-map* (kbd "C-f") "fortune")		      ;; run fortune 
(define-key *root-map* (kbd "C-l") "exec xautolock -locknow") ;; lock the screen
(define-key *root-map* (kbd "f") "fullscreen")
(define-key *root-map* (kbd "n") "fselect")
(define-key *root-map* (kbd "C-c") "exec kitty")
(define-key *root-map* (kbd "c") "exec kitty")
(define-key *root-map* (kbd "!") "exec rofi -show run")

;; Web jump (works for Google and Imdb)
;; (defmacro make-web-jump (name prefix)
;;   `(defcommand ,name (search) ((:rest ,(concatenate 'string name " search: ")))
;;      (substitute #\+ #\Space search)
;;      (run-shell-command (concatenate 'string ,prefix search))))

;; (make-web-jump "google" "firefox http://www.google.fr/search?q=")
;; (make-web-jump "imdb" "firefox http://www.imdb.com/find?q=")

;; C-t M-s is a terrble binding, but you get the idea.
;;(define-key *root-map* (kbd "M-s") "google")
;;(define-key *root-map* (kbd "i") "imdb")
