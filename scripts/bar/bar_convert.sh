#!/bin/bash
#
# Adapted from the bspwm example file

# source panel_colours

while read -r line; do
    case $line in
	WM*)
	    # bspwm
	    wm_infos=""
	    IFS=":"
	    set -- ${lines#?}
	    while [ $# -gt 0 ]; do
		item=$1
		name=${item#?}
		case $item in
		    O*)
			# focused occupied desktop
		    	wm_infos="${wm_infos}%{F#FFF6F9FF}%{BFF444444} ${name} %{B-}%{F-}"
			;;
		    U*)
			# focused urgent desktop
			wm_infos="${wm_infos}%{F#FF6F9FF}%{B#FFF9A299} ${name} %{B-}%{F-}"
			;;
		    o*)
			# Occupied desktop
			#			wm_infos="${wm_infos}%
			echo "foo"
			;;
		esac
	    done
	    ;;

	D*)
	    # Current desktop
	    desktop="${line#?}"
	    ;;
	M*)
	    # MPD info
	    mpd="${line#?}"
	    ;;
	E*)
	    # Email
	    email="${line#?}"
	    ;;
	T*)
	    # Window
	    window="${line#?}"
	    ;;
	P*)
	    # Packages
	    packages="${line#?}"
	    ;;
	V*)
	    # Volume
	    volume="${line#?}"
	    ;;
	B*)
	    # Battery
	    battery="${line#?}"
	    ;;
	C*)
	    # Clock
	    clock="${line#?}"
	    ;;
	G*)
	    # Git
	    git="${line#?}"
	    ;;
    esac
    
    printf "%s\n" "%{l}${desktop} ${wm_infos}%{c} ${window} %{r} ${mpd}    ${email}    ${git}   ${packages}  ${volume}   ${battery}    ${clock}"
done	    
