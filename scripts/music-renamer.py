# Requires the pytaglib library

from os import listdir, getcwd
from taglib import File

for song in listdir():
    try:
        artist, name = song.split("-")
    except:
        print(song)
        raise

    try:
        name, ext = song.split(".o")
    except:
        print(name + "\n")
        raise

    tag = File(song)
    tag.tags["ARTIST"] = [artist]
    tag.tags["TITLE"] = [name]
    tag.tags["ALBUM"] = getcwd().split("/")[-1]
    tag.save()
