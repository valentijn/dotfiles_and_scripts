#!/usr/bin/python3
import datetime
import eveapi
import time

eveapi.set_user_agent("conky_eve")

# API login
KEY_ID = 4943609
VCODE  = "bVHfJEuT0BUMJmC8rlDpWNRlXmKhFSy4Qor5X9aJjOBc29r321DOtk90wQVqCj4Z"

api  = eveapi.EVEAPIConnection()
auth = api.auth(keyID=KEY_ID, vCode=VCODE)
ID   = auth.char.CharacterSheet().characterID

ftime  = auth.char.SkillInTraining(characterID=ID).trainingEndTime
ctime = int(time.time())
durat = ftime - ctime

days = int(durat / 60 / 60 / 24)
durat = durat - days * 24 * 60 * 60

hours = int(durat / 60 / 60)
durat = durat - hours * 60 * 60

minutes = int(durat / 60)
durat = durat - minutes * 60

# boring time stuff
if days == 0:
    day = ""
elif days == 1:
    day = "{} day".format(days)
else:
    day = "{} days".format(days)
    
if hours == 0:
    hour = ""
elif hours == 1:
    hour = "{} hour".format(hours)
else:
    hour = "{} hours".format(hours)

if minutes == 0:
    minute = ""
elif minutes == 1:
    minute = "{} minute".format(minutes)
else:
    minute = "{} minutes".format(minutes)

'''
if durat == 0:
    second = ""
elif durat == 1:
    second = "{} second".format(durat)
else:
    second = "{} seconds".format(durat)
'''

string = ""
secondstring = ""
count = 0
long_count = 0

#for x in [day, hour, minute, second]:
for x in [day, hour, minute]:
    if x != 0:
        string = string + " " + x

for x in string.split():
    count +=  1
    long_count += 1

    if long_count == len(string.split()) - 2:
        count = 0
        secondstring = secondstring + " " + x + " and "
    elif long_count == len(string.split()):
        secondstring = secondstring + " " + x
    elif count == 2 and long_count != len(string.split()):
        count = 0
        secondstring = secondstring + " " + x + ", "
    else:
        secondstring = secondstring + x


print("{}".format(secondstring))
